${AnsiColor.BLUE} ######                                ${AnsiColor.BRIGHT_RED}   #####
${AnsiColor.BLUE} #     #  ####   ####  #    #   ##     ${AnsiColor.BRIGHT_RED}  #       #####   ####  #    # #####
${AnsiColor.BLUE} #     # #    # #      ##  ##  #  #    ${AnsiColor.BRIGHT_RED}  #       #    # #    # #    # #    #
${AnsiColor.BLUE} ######  #    #  ####  # ## # #    #   ${AnsiColor.BRIGHT_RED}  #  #### #    # #    # #    # #    #
${AnsiColor.BLUE} #       #    #      # #    # ######   ${AnsiColor.BRIGHT_RED}  #     # #####  #    # #    # #####
${AnsiColor.BLUE} #       #    # #    # #    # #    #   ${AnsiColor.BRIGHT_RED}  #     # #   #  #    # #    # #
${AnsiColor.BLUE} #        ####   ####  #    # #    #   ${AnsiColor.BRIGHT_RED}   #####  #    #  ####   ####  #

${AnsiColor.BRIGHT_BLUE}:: Posma Group 🤓  :: Running Spring Boot ${spring-boot.version} ::
:: Micro-Service ${spring.application.name} version ${info.project.version} ::
:: Posma Group - Posma-website ::${AnsiColor.DEFAULT}
