package com.posma.sckola;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.util.logging.Logger;

/**
 * Created by PosmaGroup on 02/11/2017.
 */
@SpringBootApplication(scanBasePackages = {"com.posma.sckola"},
        exclude = {SecurityAutoConfiguration.class })
public class SkolaApplication implements CommandLineRunner{
    /**
     * Main method.
     */
    private static final Logger log = Logger.getLogger("logger");

    @Autowired
    private ApplicationContext appContext;

    public static void main(String[] args) {
        SpringApplication.run(SkolaApplication.class, args);
    }

    public void run(String... strings) throws Exception {

        Environment env = this.appContext.getEnvironment();
        String protocol = "http";

        String port = env.getProperty("server.port");
        log.info("\n--------------------------------------------------------\n\tApplication '"
                + env.getProperty("spring.application.name")
                +"' is running!"
                + " Access URLs:\n\tLocal: \t\t" +protocol+
                "://localhost:"+ InetAddress.getLocalHost().getHostAddress() + ":" + port +
                "\n--------------------------------------------------------\n"
        );
    }
}
