package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.EvaluationScaleBF;
import com.posma.sckola.app.dto.EvaluationScaleDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.persistence.dao.RatingScaleDao;
import com.posma.sckola.app.persistence.entity.RatingScaleEntity;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 25/04/2017.
 */
@Service
public class EvaluationScaleBFImpl implements EvaluationScaleBF {

    @Autowired
    RatingScaleDao ratingScaleDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    /**
     * Service query all evaluation scales
     * @since 25/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto getAll() {

        MessageDto messageDto = new MessageDto();

        List<RatingScaleEntity> ratingScaleEntityList = ratingScaleDao.findAll();
        List<EvaluationScaleDto> evaluationScaleDtoList = new ArrayList<EvaluationScaleDto>(ratingScaleEntityList.size());

        for(RatingScaleEntity ratingScaleEntity:ratingScaleEntityList){
            evaluationScaleDtoList.add(entityMapper.entityToBO(ratingScaleEntity));
        }

        messageDto.setResponse(evaluationScaleDtoList);

        return messageDto;
    }

}

