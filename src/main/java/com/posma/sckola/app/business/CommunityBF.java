package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.CommunityDto;
import com.posma.sckola.app.dto.MessageDto;

import java.util.List;

/**
 * Created by Posma-ricardo on 10/04/2017.
 */
public interface CommunityBF {

    MessageDto createCommunity(CommunityDto communityDto);

    List<CommunityDto> getAll();

    MessageDto getById(Long communityId);

    MessageDto editarcommunity(Long communityId, CommunityDto communityDto);


    /**
     * Servicio que permite consultar todas las comunidades donde participa un usuarioo del sistema segun el role (TEACHER/STUDENT)
     * @param userId
     * @param roleId
     * @return MessageDto
     */
    MessageDto getByUserIdRoleId(Long userId, Long roleId);


    /**
     * Servicio que permite AsociarComunidad y Desasociar un usuario del sistema con algun role (TEACHER/STUDENT) a una comunidad
     * @param communityId
     * @param userId
     * @param roleId
     * @return MessageDto
     */
    //MessageDto setCommunityIdUserIdRoleId(Long communityId, Long userId, Long roleId, RequestAssociateDto requestAsociateDto);


}
