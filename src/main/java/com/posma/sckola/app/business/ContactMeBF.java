package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.ContactDto;
import com.posma.sckola.app.dto.MessageDto;

/**
 * Created by Posma-dev on 26/03/2020.
 */
public interface ContactMeBF {

    MessageDto SendContactMe(ContactDto contact);
}
