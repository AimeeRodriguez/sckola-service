package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.MatterCommunitySectionBF;
import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.dto.DashboardNotificationDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.NotificationHistoryDto;
import com.posma.sckola.app.dto.RequestAssociateDto;
import com.posma.sckola.app.persistence.dao.MatterCommunitySectionDao;
import com.posma.sckola.app.persistence.entity.MatterCommunitySectionEntity;
import com.posma.sckola.app.persistence.entity.StatusClass;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.NotificationMessage;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

/**
 * Created by Posma-ricardo on 01/06/2017.
 */
@Service
public class MatterCommunitySectionBFImpl implements MatterCommunitySectionBF {

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;


    @Override
    @Transactional
    public MessageDto matterCommunitySectionDesassociate(Long matterCommunitySectionId, RequestAssociateDto requestAssociateDto){

        MessageDto messageDto = new MessageDto();

        try{
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterCommunitySectionId);

            if(matterCommunitySectionEntity == null || matterCommunitySectionEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - MatterCommunitySection not exists");
                return messageDto;
            }

            if(requestAssociateDto.getAssociate()){
                matterCommunitySectionEntity.setStatus(StatusClass.ACTIVE);
            }else{
                matterCommunitySectionEntity.setStatus(StatusClass.INACTIVE);
            }

            matterCommunitySectionEntity = matterCommunitySectionDao.update(matterCommunitySectionEntity);

            messageDto.setResponse(entityMapper.entityToBO(matterCommunitySectionEntity));

            return messageDto;
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - MatterCommunitySection not exists");
            return messageDto;
        }
    }
}
