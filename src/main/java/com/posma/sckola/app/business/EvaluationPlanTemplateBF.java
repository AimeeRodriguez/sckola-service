package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;


/**
 * Created by Francis Ries on 27/04/2017.
 */
public interface EvaluationPlanTemplateBF {

    /**
     * Service query all Evaluation Plan Template
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getAll();

    /**
     * Service create new Evaluation Plan Template
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto newEvaluationPlanTemplate(Long evaluationPlanId, String evaluationPlanTemplateName);

}


