package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.QualificationUserDto;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
public interface QualificationBF {

    MessageDto createQualification(Long matterComunitySectionId, Long evaluationId);

    MessageDto updateQualification(Long qualificationId, QualificationUserDto qualificationUserDto);

    MessageDto qualificationGetStudent(Long matterComunitySectionId,Long studentId);
}
