package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.InvitationBF;
import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.dto.InvitationDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.NotificationDto;
import com.posma.sckola.app.dto.ParticipationDto;
import com.posma.sckola.app.persistence.dao.ConfMailDao;
import com.posma.sckola.app.persistence.dao.InvitationDao;
import com.posma.sckola.app.persistence.dao.ParticipationDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.ServiceProperties;
import com.posma.sckola.app.util.SystemMessage;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class InvitationBFImpl implements InvitationBF {

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    ServiceProperties serviceProperties;

    @Autowired
    UserDao userDao;

    @Autowired
    ConfMailDao confMailDao;

    @Autowired
    NotificationBF notificationBF;

    @Autowired
    InvitationDao invitationDao;

    @Autowired
    ParticipationDao participationDao;

    @Override
    @Transactional
    public MessageDto invitationCreate(InvitationDto invitationDto) {

        MessageDto messageDto = new MessageDto();
        messageDto.setMessage("invitation created");

        // valores para plantilla

        final String textKeyLogo = "<LOGO>";
        final String textKeyCode = "<CODE>";
        final String textKeyURLBase = "<URLBASE>";

        String participation = "";
        Integer counter = 1;

        for(ParticipationDto participationDto : invitationDto.getParticipation()) {
            if(counter != 1) {
                participation = participation + ", ";
            }
            participation = participation + participationDto.getName();
            counter++;
        }

        String urlBaseFront = serviceProperties.getURLServerFront(); //"http://localhost:8000/";
        String urlLogo = serviceProperties.getURLLogo(); //"http://www.posmagroup.com/assets/images/logoPosma.png";

/*        String validationText = "<h4>Validaci&oacute;n de cuenta skola</h4><br/>" +
                "Su cuenta fue creada exitosamente <br> " +
                "Para completar el registro, nacesitamos validar su cuenta <br/> Por favor has click en el siguiente enlace:<br> " +
                "<a href=\"http://<IPHOST>:8080/skola/user/validate?code=<code>\"> http://<IPHOST>:8080/skola/user/validate?code=<code></a> <br> " +
                "Si en link no funciona, copia el enlace y pegalo en una nueva ventada del browser";

                String validationText2 = "<h4>Validaci&oacute;n de cuenta skola</h4><br/>" +
                "Su cuenta fue creada exitosamente <br> " +
                "Para completar el registro, nacesitamos validar su cuenta <br/> Por favor has click en el siguiente enlace:<br> " +
                "<a href=\""+urlLogin+"/#/user/validate?code="+textKeyCode+"\"> "+urlLogin+"/#/user/validate?code="+textKeyCode+"></a> <br> " +
                "Si en link no funciona, copia el enlace y pegalo en una nueva ventada del browser";
        */

        String validationText = "<table border=0 cellspacing=0 cellpadding=0 width=512 bgcolor=#000000\n" +
                "       style=\"background-color:#f0f0f0;margin:0 auto;max-width:512px;width:inherit\">\n" +
                "    <tbody>\n" +
                "    <tr>\n" +
                "        <td bgcolor=\"#F6F8FA\" style=\"background-color:#f6f8fa;padding:28px 0 20px 0\">\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"\n" +
                "                   style=\"width:100%!important;min-width:100%!important\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\">\n" +
                "                        <a href=\""+textKeyURLBase+"\"\n" +
                "                           style=\"color:#008cc9;display:inline-block;text-decoration:none\" target=\"_blank\" data-saferedirecturl=\""+urlBaseFront+"\">\n" +
                "                            <img alt=\"Skola\" border=\"0\" src=\""+textKeyLogo+"\" height=\"60\" width=\"68\"\n" +
                "                                 style=\"outline:none;color:#ffffff;text-decoration:none\">\n" +
                "                        </a>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\" style=\"padding:16px 24px 0 24px\">\n" +
                "                        <h2 style=\"margin:0;color:#262626;font-weight:200;font-size:20px;padding-bottom:5px;line-height:1.2\">Solicitud de Invitaci&oacute;n</h2>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"margin:0 10px;max-width:492px\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" style=\"padding:25px 0;text-align:center\">\n" +
                "                        <p style=\"margin:0;color:#262626;font-weight:100;font-size:16px;padding-bottom:15px;line-height:1.167\">Invitaci&oacute;n<br/>\n" +
                "                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline-block\">\n" +
                "                            <tbody>\n" +
                "                            <tr>\n" +
                "                                <td>"+
                "                                   Nombre Completo: "+
                "                                </td>\n" +
                "                                <td>"+
                "                                   "+ invitationDto.getName()+
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            <tr>\n" +
                "                                <td>"+
                "                                   Correo Electronico: "+
                "                                </td>\n" +
                "                                <td>"+
                "                                   "+ invitationDto.getMail()+
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            <tr>\n" +
                "                                <td>"+
                "                                   Telefono: "+
                "                                </td>\n" +
                "                                <td>"+
                "                                   "+ invitationDto.getPhone()+
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            <tr>\n" +
                "                                <td>"+
                "                                   Ocupación: "+
                "                                </td>\n" +
                "                                <td>"+
                "                                   "+ invitationDto.getOcupation()+
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            <tr>\n" +
                "                                <td>"+
                "                                   Participa como: "+
                "                                </td>\n" +
                "                                <td>"+
                    "                                   "+ participation+
                    "                                </td>\n" +
                    "                            </tr>\n" +
                    "                            <tr>\n" +
                    "                                <td>"+
                    "                                   Nombre de la institución: "+
                    "                                </td>\n" +
                    "                                <td>"+
                    "                                   "+ invitationDto.getInstitution()+
                    "                                </td>\n" +
                    "                            </tr>\n" +
                    "                            <tr>\n" +
                    "                                <td>"+
                    "                                   ¿Que desea hacer en Sckola?: "+
                    "                                </td>\n" +
                    "                                <td>"+
                    "                                   "+ invitationDto.getOther()+
                    "                                </td>\n" +
                    "                            </tr>\n" +
                    "                            </tbody>\n" +
                    "                        </table>\n" +
                    "                    </td>\n" +
                    "                </tr>\n" +
                    "                </tbody>\n" +
                    "            </table>\n" +
                    "        </td>\n" +
                    "    </tr>\n" +
                    "    </tbody>\n" +
                    "</table>;\n";



        try{
            if(userDao.findByMail(invitationDto.getMail().toUpperCase()) != null){
                messageDto.setSuccess(false);
                messageDto.setMessage("Already exists user with the mail " + invitationDto.getMail());
                return messageDto;
            }
            if(invitationDao.findByMail(invitationDto.getMail().toUpperCase()) != null){
                messageDto.setSuccess(false);
                messageDto.setMessage("Already exists invitation with the mail " + invitationDto.getMail());
                return messageDto;
            }
            if (invitationDto != null && invitationDto.getMail() != null && invitationDto.getMail().trim() != "") {

                InvitationEntity invitationEntity = new InvitationEntity();

                invitationEntity.setName(invitationDto.getName());
                invitationEntity.setMail(invitationDto.getMail().toUpperCase());
                invitationEntity.setInstitution(invitationDto.getInstitution());
                invitationEntity.setPhone(invitationDto.getPhone());
                invitationEntity.setOcupation(invitationDto.getOcupation());
                invitationEntity.setOther(invitationDto.getOther());

                invitationEntity = invitationDao.create(invitationEntity);

                for(ParticipationDto participationDto: invitationDto.getParticipation()){
                    ParticipationEntity participationEntity = new ParticipationEntity();

                    participationEntity.setName(participationDto.getName());
                    participationEntity.setInvitation(invitationEntity);

                    participationDao.create(participationEntity);
                }

                //Preparar contenido del correo
                validationText = validationText.replaceAll(textKeyURLBase, urlBaseFront);
                validationText = validationText.replaceAll(textKeyLogo, urlLogo);


                NotificationDto notificationDto = new NotificationDto();
                notificationDto.setText(validationText);
                notificationDto.setTitle("Correo de solicitud de invitación");
                notificationDto.setTo(confMailDao.findOne(2).getEmail());
                // Envia correo para la validacion de cuenta

                messageDto.setResponse("Sucefull");
                notificationBF.sendNotification(notificationDto);

            }else{
                messageDto.setSuccess(false);
                messageDto.setMessage("Bad input to send mail, It was not possible to send the mail");
            }

        }catch (DataIntegrityViolationException dve){
            dve.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Already exists user with the mail " + invitationDto.getMail());
            new Exception(messageDto.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Internal Exception, please contact yuor administrator");
            new Exception(messageDto.getMessage());
        }

        return messageDto;
    }
}
