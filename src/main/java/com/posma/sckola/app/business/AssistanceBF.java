package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.AssistanceDto;
import com.posma.sckola.app.dto.MessageDto;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
public interface AssistanceBF {

    MessageDto createAssistance(Long matterComunitySection, AssistanceDto assistanceDto);

    MessageDto updateAssistance(AssistanceDto assistanceDto);

    MessageDto getAllDate(Long matterComunitySectionId,String date);
}
