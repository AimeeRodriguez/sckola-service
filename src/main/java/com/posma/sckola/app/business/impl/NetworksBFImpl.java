package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.NetworksBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.NetworksDto;
import com.posma.sckola.app.dto.UserDto;
import com.posma.sckola.app.persistence.dao.NetworkDao;
import com.posma.sckola.app.persistence.entity.NetworksEntity;
import com.posma.sckola.app.util.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Posma-dev on 07/09/2018.
 */
@Service
public class NetworksBFImpl implements NetworksBF {

    @Autowired
    NetworkDao networkDao;

    @Autowired
    EntityMapper entityMapper;

    public MessageDto GetAllNetworksUser(UserDto userDto){
        MessageDto messageDto = new MessageDto();
        List<NetworksDto> networksDtos = new ArrayList<NetworksDto>();
        try {
            List<NetworksEntity> networksEntities = networkDao.networksByUser(userDto.getId());

            for (NetworksEntity networksEntity: networksEntities){
                networksDtos.add(entityMapper.entityToBO(networksEntity));
            }

            messageDto.setResponse(networksDtos);
            return messageDto;

        }catch (Exception e){
            messageDto.setResponse(networksDtos);
            messageDto.setSuccess(false);
            return messageDto;
        }
    }
}
