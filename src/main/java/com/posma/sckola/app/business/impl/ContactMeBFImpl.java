package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.business.ContactMeBF;
import com.posma.sckola.app.dto.ContactDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.NotificationDto;
import com.posma.sckola.app.persistence.dao.ConfMailDao;
import com.posma.sckola.app.persistence.entity.ConfMailEntity;
import com.posma.sckola.app.util.ServiceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Posma-dev on 26/03/2020.
 */
@Service
public class ContactMeBFImpl implements ContactMeBF {

    @Autowired
    NotificationBF notificationBf;

    @Autowired
    ConfMailDao confMailDao;

    @Autowired
    ServiceProperties serviceProperties;

    @Override
    @Transactional
    public MessageDto SendContactMe(ContactDto contact){
        MessageDto messageDto = new MessageDto();

        ConfMailEntity mailEntity = confMailDao.findOne(1);

        final String textKeyLogo = "<LOGO>";
        final String textKeyCode = "<CODE>";
        final String textKeyURLBase = "<URLBASE>";
        final String textContent = "<CONTENT>";
        final String textName = "<NAME>";
        final String textEmail = "<EMAIL>";

        String urlBaseFront = serviceProperties.getURLServerFront(); //"http://localhost:8000/";
        String urlLogo = serviceProperties.getURLLogo(); //"http://www.posmagroup.com/assets/images/logoPosma.png";
        String content = contact.getMessage();
        String name = contact.getName();
        String email = contact.getEmail();
        System.out.println(name);
        System.out.println(content);
        System.out.println(email);

        String validationText = "<table border=0 cellspacing=0 cellpadding=0 width=700 bgcolor=#000000\n" +
                "       style=\"background-color:#f0f0f0;margin:0 auto;max-width:700px;\">\n" +
                "    <tbody>\n" +
                "    <tr>\n" +
                "        <td bgcolor=\"#F6F8FA\" style=\"background-color:#f6f8fa;padding:28px 0 20px 0\">\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"\n" +
                "                   style=\"width:100%!important;min-width:100%!important\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\">\n" +
                "                        <a href=\""+textKeyURLBase+"\"\n" +
                "                           style=\"color:#008cc9;display:inline-block;text-decoration:none\" target=\"_blank\" data-saferedirecturl=\""+urlBaseFront+"\">\n" +
                "                            <img alt=\"Skola\" border=\"0\" src=\""+textKeyLogo+"\" height=\"60\" width=\"68\"\n" +
                "                                 style=\"outline:none;color:#ffffff;text-decoration:none\">\n" +
                "                        </a>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\" style=\"padding:16px 24px 0 24px\">\n" +
                "                        <h2 style=\"margin:0;color:#262626;font-weight:200;font-size:20px;padding-bottom:5px;line-height:1.2\">Mensaje de "+ textName+
                "                           </h2>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td align=\"center\">\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"margin:0 10px;max-width:492px\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" style=\"padding:25px 0;text-align:center\">\n" +
                "                        <p style=\"margin:0;color:#262626;font-weight:100;font-size:16px;padding-bottom:15px;line-height:1.167\">Correo: " + textEmail+
                "                               <br/>\n" +
                "                            <br/></p>\n" +
                "                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline-block\">\n" +
                "                            <tbody>\n" +
                "                            <tr>\n" +
                "                                <td align=\"center\" valign=\"middle\">\n" +
                "                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"auto\">\n" +
                "                                            <tbody>\n" +
                "                                            <tr>\n" +
                "                                                <td bgcolor=\"#008CC9\" style=\"padding:6px 16px;color:#ffffff;;font-weight:bold;font-size:16px;border-color:#008cc9;background-color:#008cc9;border-radius:2px;border-width:1px;border-style:solid\">\n" +
                                                                    textContent+
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "                                            </tbody>\n" +
                "                                        </table>\n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            </tbody>\n" +
                "                        </table>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    </tbody>\n" +
                "</table>;\n";

        //Preparar contenido del correo
        validationText = validationText.replaceAll(textContent, content);
        validationText = validationText.replaceAll(textName, name);
        validationText = validationText.replaceAll(textEmail, email);
        validationText = validationText.replaceAll(textKeyURLBase, urlBaseFront);
        validationText = validationText.replaceAll(textKeyLogo, urlLogo);

        NotificationDto notificationDto = new NotificationDto();
        notificationDto.setText(validationText);
        notificationDto.setTitle("Contactarme de " + contact.getName());
        notificationDto.setTo(mailEntity.getEmail());

        try{
            Boolean sendStatus = notificationBf.sendNotification(notificationDto);
            messageDto.setSuccess(sendStatus);
            if(sendStatus){
                messageDto.setResponse("Se envio el mensaje");
            }else{
                messageDto.setResponse("Dio error al enviar el mensaje");
            }
        }catch (DataIntegrityViolationException dve){
            dve.printStackTrace();
            messageDto.setSuccess(false);
            new Exception(messageDto.getMessage());
        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage("Internal Exception, please contact yuor administrator");
            new Exception(messageDto.getMessage());
        }

        return messageDto;
    }
}
