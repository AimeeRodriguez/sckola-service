package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.SectionDto;


/**
 * Created by Francis Ries on 24/04/2017.
 */
public interface SectionBF {

    /**
     * Service query all sections from a community
     * @since 24/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto sectionCommunityGetAll(Long communityId);

    /**
     * Service create section in a community
     * @since 24/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto sectionCommunityCreate(Long communityId,  SectionDto sectionDto, Long idUser);

    /**
     * Consultar el listado de estudiantes de una seccion
     * @since 25/04/2017
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getAllUserSection(Long sectionId);

    /**
     * Add user to a section
     * @since 25/04/2017
     * @param userId user identifier
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto associateUserSection(Long userId, Long sectionId);


    /**
     * remove user from a section
     * @since 25/04/2017
     * @param userId user identifier
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto desassociateUserSection(Long userId, Long sectionId);
}


