package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.EvaluationPlanBF;
import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.dto.DashboardNotificationDto;
import com.posma.sckola.app.dto.EvaluationPlanDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.NotificationHistoryDto;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.NotificationMessage;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 26/04/2017.
 */
@Service
public class EvaluationPlanBFImpl implements EvaluationPlanBF {

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    UserDao userDao;

    @Autowired
    WizardDao wizardDao;

    @Autowired
    EvaluationPlanTemplateDao evaluationPlanTemplateDao;

    @Autowired
    NotificationBF notificationBf;

    @Autowired
    NotificationMessage notificationMessage;
    /**
     * Service query all evaluation plans from teacher in a community
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto getAllFromTeacherBycommunity(Long userId, Long communityId) {

        MessageDto messageDto = new MessageDto();

        //Busca todas las evaluacionas pertenecientes a una comunidad
        List<EvaluationPlanEntity> evaluationPlanEntityList = evaluationPlanDao.findAllByTeacherCommunity(communityId, userId, Status.ACTIVE);
        List<EvaluationPlanDto> evaluationPlanDtoList = new ArrayList<EvaluationPlanDto>(evaluationPlanEntityList.size());

        for(EvaluationPlanEntity evaluationPlanEntity:evaluationPlanEntityList){
            evaluationPlanDtoList.add(entityMapper.entityToBO(evaluationPlanEntity));
        }

        //Busca todas los planes de evaluaciones que aun no han sido asignados a ninguna materia
        evaluationPlanDtoList.addAll((List<EvaluationPlanDto>) getAllFromTeacherNoAssociate(userId).getResponse());

        messageDto.setResponse(evaluationPlanDtoList);

        return messageDto;
    }


    /**
     * Service query all evaluation plans from teacher that not associate
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto getAllFromTeacherNoAssociate(Long userId) {

        MessageDto messageDto = new MessageDto();

        List<EvaluationPlanEntity> evaluationPlanEntityList = evaluationPlanDao.findAllByTeacherNotAssociate(userId);
        List<EvaluationPlanDto> evaluationPlanDtoList = new ArrayList<EvaluationPlanDto>(evaluationPlanEntityList.size());

        for(EvaluationPlanEntity evaluationPlanEntity:evaluationPlanEntityList){
            evaluationPlanDtoList.add(entityMapper.entityToBO(evaluationPlanEntity));
        }

        messageDto.setResponse(evaluationPlanDtoList);

        return messageDto;
    }

    /**
     * Service query Evaluation Plan detail by ID
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto getById(Long evaluationPlanId) {

        MessageDto messageDto = new MessageDto();

        EvaluationPlanEntity evaluationPlanEntity = evaluationPlanDao.findOne(evaluationPlanId);

        if(evaluationPlanEntity == null){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-001");
            messageDto.setMessage(systemMessage.getMessage("SK-001"));
            return messageDto;
        }

        messageDto.setResponse(entityMapper.entityToBOFull(evaluationPlanEntity));

        return messageDto;
    }



    /**
     * Service create new Evaluation Plan
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto newEvaluationPlan(EvaluationPlanDto evaluationPlanDto) {

        MessageDto messageDto = new MessageDto();

        EvaluationPlanEntity evaluationPlanEntity = new EvaluationPlanEntity();
        WizardEntity wizardEntity;
        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", evaluationPlanDto.getUserId());

        try {
            evaluationPlanEntity.setName(evaluationPlanDto.getName());
            evaluationPlanEntity.setUser(userDao.findOne(evaluationPlanDto.getUserId()));

            List<EvaluationPlanEntity> evaluationPlanEntityList = evaluationPlanDao.findAllByTeacher(evaluationPlanEntity.getUser().getId(), Status.ACTIVE);

            for(EvaluationPlanEntity evaluationPlanEntity1: evaluationPlanEntityList){
                if(evaluationPlanEntity1.getName()!=null && evaluationPlanEntity1.getName().equalsIgnoreCase(evaluationPlanEntity.getName())){
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("SK-005");
                    messageDto.setMessage(systemMessage.getMessage("SK-005") + " - Ya existe un plan de evaluación con el mismo nombre '"+ evaluationPlanEntity.getName() +"'");
                    return messageDto;
                }
            }

            evaluationPlanEntity = evaluationPlanDao.create(evaluationPlanEntity);

            if(wizardEntityList.size() >= 1) {
                wizardEntity = wizardEntityList.get(0);

                EvaluationPlanEntity evaluationPlan = evaluationPlanDao.findOne(evaluationPlanEntity.getId());
                wizardEntity.setEvaluationPlan(evaluationPlanEntity);
                wizardEntity = wizardDao.update(wizardEntity);
                entityMapper.entityToBO(wizardEntity);
            }


            if (evaluationPlanEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            //oculta el mensaje de solicitud de plan de evaluacion
            MessageDto notification = notificationBf.getHistoryNotificationByInfo(evaluationPlanEntity.getUser().getMail());
            NotificationHistoryDto historyDto = (NotificationHistoryDto) notification.getResponse();

            for (DashboardNotificationDto dashboardNotificationDto: historyDto.getNotifications()) {
                if(dashboardNotificationDto.getFrom().equals(notificationMessage.getEvaluationPlan()))
                    notificationBf.removeActiveNotification(dashboardNotificationDto.getId());

            }

            messageDto.setResponse(entityMapper.entityToBO(evaluationPlanEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }


    /**
     * Servicio que permite asociar un plan de evaluación a una materia de la comunidad
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto associateEvaluationPlanClass(Long planId, Long matterCommunitySectionId, boolean associate){

        MessageDto messageDto = new MessageDto();

        MatterCommunitySectionEntity matterCommunitySectionEntity;
        EvaluationPlanEntity evaluationPlanEntity;

        try{
            evaluationPlanEntity = evaluationPlanDao.findOne(planId.longValue());
            matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterCommunitySectionId.longValue());

            if(evaluationPlanEntity == null || matterCommunitySectionEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Evaluation Plan or Matter_Community_Section not exists");
                return messageDto;
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Evaluation Plan or Matter_Community_Section not exists");
            return messageDto;
        }

        //associate ?
        if(associate){
            // Validar que no exista plan de evaluacion a la clase que se quiere asociar el plan
            if(matterCommunitySectionEntity.getEvaluationPlan() != null){
                if(matterCommunitySectionEntity.getEvaluationPlan().getId() == planId){
                    messageDto.setMessage("Plan already was associate");
                    messageDto.setResponse(entityMapper.entityToBO(matterCommunitySectionEntity));
                    return messageDto;
                }
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-005");
                messageDto.setMessage(systemMessage.getMessage("SK-005")+ " - The Class already have a Evaluation Plan");
                return messageDto;
            }

            matterCommunitySectionEntity.setEvaluationPlan(evaluationPlanEntity);
            matterCommunitySectionEntity = matterCommunitySectionDao.update(matterCommunitySectionEntity);

            //evaluationPlanEntity.setMatter(matterCommunitySectionEntity);
            //evaluationPlanDao.update(evaluationPlanEntity);

            messageDto.setResponse(entityMapper.entityToBO(matterCommunitySectionEntity));

        }else{
            // disassociate

            // Validar que exista plan de evaluacion a la clase que se quiere desasociar
            if(matterCommunitySectionEntity.getEvaluationPlan() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-006");
                messageDto.setMessage(systemMessage.getMessage("SK-006") + " - The Class haven't a Evaluation Plan");
                return messageDto;
            }else{
                matterCommunitySectionEntity.setEvaluationPlan(null);
                matterCommunitySectionEntity = matterCommunitySectionDao.update(matterCommunitySectionEntity);
            }

            messageDto.setResponse(entityMapper.entityToBO(matterCommunitySectionEntity));
        }

        return messageDto;
    }

    /*
     * Create new evaluation plan from a evaluation plan template
     * @since 28/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto newEvaluationPlanFromTemplate(Long planTemplateIdToCopy) {

        MessageDto messageDto = new MessageDto();

        try {
            EvaluationPlanTemplateEntity evaluationPlanTemplateEntity = evaluationPlanTemplateDao.findOne(planTemplateIdToCopy);

            if(evaluationPlanTemplateEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            // Realiza la copia del plan de evaluaciones
            EvaluationPlanEntity evaluationPlanEntity = copy(evaluationPlanTemplateEntity);


            evaluationPlanEntity = evaluationPlanDao.create(evaluationPlanEntity);

            if (evaluationPlanEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBOFull(evaluationPlanEntity));

        }catch(NoResultException ne){
            ne.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto updateEvaluationPlan(Long planTemplateId, EvaluationPlanDto evaluationPlanDto){

        MessageDto messageDto = new MessageDto();

        try {
            EvaluationPlanEntity evaluationPlanEntity = evaluationPlanDao.findOne(planTemplateId);
            UserEntity userEntity = userDao.findOne(evaluationPlanDto.getUserId());

            if(evaluationPlanEntity == null || userEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-003");
                messageDto.setMessage(systemMessage.getMessage("SK-003"));
            }

            evaluationPlanEntity.setName(evaluationPlanDto.getName());
            evaluationPlanEntity.setUser(userEntity);
            evaluationPlanEntity = evaluationPlanDao.update(evaluationPlanEntity);

            messageDto.setResponse(entityMapper.entityToBOFull(evaluationPlanEntity));

        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }


    private EvaluationPlanEntity copy(EvaluationPlanTemplateEntity evaluationPlanTemplateEntity){
        if(evaluationPlanTemplateEntity == null)
            return null;
        EvaluationPlanEntity evaluationPlanEntity = new EvaluationPlanEntity();
        evaluationPlanEntity.setName(evaluationPlanTemplateEntity.getName());
        evaluationPlanEntity.setUser(evaluationPlanTemplateEntity.getUser());
        if(evaluationPlanTemplateEntity.getEvaluationTemplateList() != null) {
            for(EvaluationTemplateEntity evaluationTemplateEntity: evaluationPlanTemplateEntity.getEvaluationTemplateList())
                evaluationPlanEntity.addEvaluationList(this.copy(evaluationTemplateEntity, evaluationPlanEntity));
        }
        return evaluationPlanEntity;
    }

    private EvaluationEntity copy(EvaluationTemplateEntity evaluationTemplateEntity, EvaluationPlanEntity evaluationPlanEntity){
        EvaluationEntity evaluationEntity = new EvaluationEntity();
        //evaluationTemplateEntity.setId(evaluationEntity.getId());
        evaluationEntity.setObjective(evaluationTemplateEntity.getObjective());
        evaluationEntity.setDate(evaluationTemplateEntity.getDate());
        evaluationEntity.setWeight(evaluationTemplateEntity.getWeight());
        evaluationEntity.setStatus(evaluationTemplateEntity.getStatus());
        evaluationEntity.setEvaluationTool(evaluationTemplateEntity.getEvaluationTool());
        evaluationEntity.setRatingScale(evaluationTemplateEntity.getEvaluationScale());
        evaluationEntity.setEvaluationPlan(evaluationPlanEntity);

        return evaluationEntity;
    }

    /*
    private EvaluationValueTemplateEntity copy(RatingValueEntity ratingValueEntity, EvaluationScaleTemplateEntity evaluationScaleTemplateEntity){
        EvaluationValueTemplateEntity evaluationValueTemplateEntity = new EvaluationValueTemplateEntity();
        //evaluationValueTemplateEntity.setId(ratingValueEntity.getId());
        evaluationValueTemplateEntity.getQualification().setText(ratingValueEntity.getQualification().getText());
        evaluationValueTemplateEntity.getQualification().setValue(ratingValueEntity.getQualification().getValue());
        evaluationValueTemplateEntity.getQualification().setWeight(ratingValueEntity.getQualification().getWeight());
        evaluationValueTemplateEntity.setEvaluationScaleTemplate(evaluationScaleTemplateEntity);
        return evaluationValueTemplateEntity;
    }

    private EvaluationScaleTemplateEntity copy(RatingScaleEntity ratingScaleEntity){
        EvaluationScaleTemplateEntity evaluationScaleTemplateEntity = new EvaluationScaleTemplateEntity();
        //evaluationScaleTemplateEntity.setId(ratingScaleEntity.getId());
        evaluationScaleTemplateEntity.setName(ratingScaleEntity.getName());
        evaluationScaleTemplateEntity.setDescription(ratingScaleEntity.getDescription());
        for(RatingValueEntity ratingValueEntity: ratingScaleEntity.getRatingValueList()){
            evaluationScaleTemplateEntity.addEvaluationValueList(this.copy(ratingValueEntity, evaluationScaleTemplateEntity));
        }
        return evaluationScaleTemplateEntity;
    }

    private EvaluationToolTemplateEntity copy(EvaluationToolEntity evaluationToolEntity){
        EvaluationToolTemplateEntity evaluationToolTemplateEntity = new EvaluationToolTemplateEntity();
        //evaluationToolTemplateEntity.setId(evaluationToolEntity.getId());
        evaluationToolTemplateEntity.setName(evaluationToolEntity.getName());
        evaluationToolTemplateEntity.setDescription(evaluationToolEntity.getDescription());
        return evaluationToolTemplateEntity;
    }
    */


}

