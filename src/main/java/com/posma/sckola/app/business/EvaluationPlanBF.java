package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.EvaluationPlanDto;
import com.posma.sckola.app.dto.MessageDto;


/**
 * Created by Francis Ries on 26/04/2017.
 */
public interface EvaluationPlanBF {

    /**
     * Service query all Evaluation Plan from a teacher in a community
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getAllFromTeacherBycommunity(Long userId, Long communityId);;

    /**
     * Service query a Evaluation Plan by ID
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getById(Long evaluationPlanId);

    /**
     * Service create new Evaluation Plan
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto newEvaluationPlan(EvaluationPlanDto evaluationPlanDto);

    /**
     * Service query all evaluation plans from teacher that not associate
     * @since 26/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getAllFromTeacherNoAssociate(Long userId);


    /**
     * Servicio que permite asociar un plan de evaluación a una materia de la comunidad
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto associateEvaluationPlanClass(Long planId, Long matterCommunitySectionId, boolean associate);

    /**
     * Create new evaluation plan from a evaluation plan template
     * @since 28/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto newEvaluationPlanFromTemplate(Long planTemplateIdToCopy);

    MessageDto updateEvaluationPlan(Long planTemplateId, EvaluationPlanDto evaluationPlanDto);

}


