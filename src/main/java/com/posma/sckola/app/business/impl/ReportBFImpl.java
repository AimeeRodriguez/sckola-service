package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.ReportBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.QualificationUserDto;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Posma-ricardo on 01/11/2017.
 */
@Service
public class ReportBFImpl implements ReportBF {

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @Autowired
    QualificationUserDao qualificationUserDao;

    @Autowired
    MatterCommunityDao matterCommunityDao;

    @Autowired
    SectionDao sectionDao;

    @Autowired
    AssistanceByMatterDao assistanceByMatterDao;

    @Autowired
    AssistanceIntegralDao assistanceIntegralDao;

    @Autowired
    ReportDao reportDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    Validation validation;

    @Override
    @Transactional
    public MessageDto reportMatterSectionAverage(Long matterCommunitySectionId) {

        MessageDto messageDto = new MessageDto();
        JSONObject object = new JSONObject();

        try {
            int Response = 0;
            int CountQualification;
            int Count;
            int CountEvaluation = 0;
            int RatingValueFin = 0;
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterCommunitySectionId);
            EvaluationPlanEntity evaluationPlanEntity = evaluationPlanDao.findOne(matterCommunitySectionEntity.getEvaluationPlan().getId());

            if (matterCommunitySectionEntity == null || evaluationPlanEntity == null) {
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002") + " - MatterCommunitySection or evaluationPlan not exists");
                return messageDto;
            }

            for (EvaluationEntity evaluationEntity : evaluationPlanEntity.getEvaluationList()) {
                if (evaluationEntity.getStatus() == StatusEvaluation.CULMINATED || evaluationEntity.getStatus() == StatusEvaluation.IN_PROGRESS) {
                    List<QualificationUserEntity> qualificationUserEntities = qualificationUserDao.getAllToEvaluation(evaluationEntity.getId());
                    Count = 0;
                    CountQualification = 0;
                    for (QualificationUserEntity qualificationUserEntity : qualificationUserEntities) {
                        if (qualificationUserEntity.getQualification() != null)
                            CountQualification = CountQualification + Integer.parseInt(qualificationUserEntity.getQualification().getValue());
                        Count = Count + 1;
                    }
                    Response = Response + (CountQualification / Count);
                }
                for(RatingValueEntity ratingValueEntity: evaluationEntity.getRatingScale().getRatingValueList()){
                    int RatingValue = Integer.parseInt(ratingValueEntity.getQualification().getValue());
                    if(RatingValue > RatingValueFin){
                        RatingValueFin = RatingValue;
                    }
                }
                CountEvaluation = CountEvaluation + 1;
            }

            Response = Response / CountEvaluation;
            object.put("Result", Response);
            object.put("RatingValue", RatingValueFin);
            messageDto.setResponse(object);

        } catch (NoResultException e) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - MatterCommunitySection or evaluationPlan not exists");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - MatterCommunitySection or evaluationPlan not exists");
        }

        return messageDto;
    }

    /**
     * obtiene la lista de las evaluaciones de un plan de evaluacion
     * @param communityId
     * @param matterId
     * @param sectionId
     * @param userId
     * @return
     */
    @Override
    @Transactional
    public MessageDto reportPlanEvaluation (Long communityId, Long matterId, Long sectionId, Long userId) {
        MessageDto messageDto = new MessageDto();

        JSONObject object = new JSONObject();
        JSONArray matters = new JSONArray();

        List<EvaluationPlanEntity> evaluationPlanEntity = new ArrayList<>();
        List<EvaluationEntity> evaluations = new ArrayList<>();

        try {

           evaluationPlanEntity = evaluationPlanDao.findByMatterSectionCommunity(communityId, matterId, sectionId, userId);

            if(evaluationPlanEntity == null || evaluationPlanEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - MatterCommunitySection or evaluationPlan not exists");
                return messageDto;
            }else {
                List<EvaluationEntity> evaluationEntityList =  evaluationPlanEntity.get(0).getEvaluationList();

                for (EvaluationEntity evaluationEntity: evaluationEntityList){
                    JSONObject matter = new JSONObject();
                    matter.put("id", evaluationEntity.getId());
                    matter.put("name",evaluationEntity.getEvaluationTool().getName());
                    matter.put("weight", evaluationEntity.getWeight());
                    matters.add(matter);
                }
                object.put("name", evaluationPlanEntity.get(0).getName());
                object.put("evaluaciones", matters);
            }

            messageDto.setResponse(object);

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - MatterCommunitySection or evaluationPlan not exists");
        }

        return messageDto;
    }

    /**
     * Obtiene la lista de las materias de una comunidad
     * @param communityId
     * @return
     */
    @Override
    @Transactional
    public MessageDto getMatterByCommunityUser (Long communityId, Long userId) {

        MessageDto messageDto = new MessageDto();

        JSONObject object = new JSONObject();
        List<MatterCommunityEntity> matterCommunityEntityList = new ArrayList<>();

        try {
            matterCommunityEntityList = matterCommunityDao.findAllMatterByCommunityIdUserId(communityId, userId);
            object.put("matters",matterCommunityEntityList);

            messageDto.setResponse(object);

        }catch (NoResultException e) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - MatterCommunitySection or evaluationPlan not exists");
        }

        return messageDto;
    }

    /**
     * obtiene las secciones por materia y comunidad
     * @param matterId
     * @param communityId
     * @return
     */
    @Override
    @Transactional
    public MessageDto getSectionByMatterCommunity(Long matterId, Long communityId) {

        MessageDto messageDto = new MessageDto();

        JSONObject matters = new JSONObject();
        JSONArray result = new JSONArray();
        List<SectionEntity> sectionMatterEntityList = new ArrayList<>();

        try {
            sectionMatterEntityList = sectionDao.findAllByMatterCommunity(matterId, communityId);

            for (SectionEntity sectionEntity:sectionMatterEntityList) {
                JSONObject object = new JSONObject();
                object.put("id",sectionEntity.getId());
                object.put("name",sectionEntity.getName());
                result.add(object);
            }

            matters.put("sections",result);

            messageDto.setResponse(matters);

        }catch (NoResultException e) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - SectionByMatterCommunity not exists");
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto  studentsMatterSection(Long sectionId, Long matterId){
        MessageDto messageDto = new MessageDto();
        JSONObject object = new JSONObject();

        try{
           List<UserEntity> userEntityList = reportDao.studentsMatterSection(sectionId,matterId);
            if(userEntityList == null ){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Section not exists");
                return messageDto;
            }
            for(UserEntity userEntity: userEntityList){
                userEntity.setUserRoleCommunityList(null);
            }
            object.put("Result", userEntityList);
            messageDto.setResponse(object);
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Section not exists");
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto  sectionsByMatterCommunity(Long matterId, Long communityId, Long teacherId){
        MessageDto messageDto = new MessageDto();
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();

        try{
           List<SectionEntity> sectionEntityList = reportDao.sectionsByMatterCommunity(matterId, communityId, teacherId);
            if(sectionEntityList == null ){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
                return messageDto;
            }

            for(SectionEntity sectionEntity : sectionEntityList){
                JSONObject entity = new JSONObject();
                entity.put("id",sectionEntity.getId());
                entity.put("name", sectionEntity.getName());
                array.add(entity);
            }
            object.put("Result", array);
            messageDto.setResponse(object);
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Section not exists");
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto matterByCommunityTeacher(Long communityId, Long teacherId ){
        MessageDto messageDto = new MessageDto();
        JSONObject object = new JSONObject();
        JSONArray array = new JSONArray();
        try{
           List<MatterEntity> matterEntityList = reportDao.matterByCommunityTeacher(communityId, teacherId);
            if(matterEntityList == null ){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
                return messageDto;
            }
            for(MatterEntity matterEntity : matterEntityList){
                JSONObject entity = new JSONObject();
                entity.put("id", matterEntity.getId());
                entity.put("name", matterEntity.getName());
                array.add(entity);
            }
            object.put("Result", array);
            messageDto.setResponse(object);
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Community not exists");
        }

        return messageDto;
    }

    public MessageDto reportNoAttendance(Long matterCommunitySectionId) {
        MessageDto messageDto = new MessageDto();
        JSONObject object = new JSONObject();
        JSONObject value = new JSONObject();
        List datesClass = new ArrayList<>();
        List totalForDate = new ArrayList<>();
        int noAsistance =0;
        int date=0;


        MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterCommunitySectionId);
        if(matterCommunitySectionEntity == null || matterCommunitySectionEntity.getSection() == null){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return messageDto;
        }

        SectionEntity sectionEntity = matterCommunitySectionEntity.getSection();
        if(matterCommunitySectionEntity.getType().name().equals("NON_INTEGRAL")){
            AssistanceByMatterPk assistanceByMatterPk = new AssistanceByMatterPk();
            assistanceByMatterPk.setMatterCommunitySection(matterCommunitySectionId);
            try {
                List<AssistanceByMatterEntity> assistanceByMatterEntityList = assistanceByMatterDao.noAttendanceFindAllNonIntegral(assistanceByMatterPk);
                for(AssistanceByMatterEntity assistanceByMatterEntity1: assistanceByMatterEntityList){
                    if (!datesClass.contains(assistanceByMatterEntity1.getId().getDate())){
                        datesClass.add(assistanceByMatterEntity1.getId().getDate());
                    }
                }
                for (int i =0;i<datesClass.size();i++){
                    for (AssistanceByMatterEntity assistanceByMatterEntity1: assistanceByMatterEntityList){
                        if (datesClass.get(i).equals(assistanceByMatterEntity1.getId().getDate())){
                            date++;
                            if (!assistanceByMatterEntity1.getAttended().booleanValue()){
                                noAsistance++;
                            }
                        }
                    }
                    value.put("Date",datesClass.get(i));
                    value.put("Cantidad",date);
                    value.put("inasistencia",noAsistance);
                    date=0;
                    noAsistance=0;
                    totalForDate.add(value);
                    value = new JSONObject();
                }

                object.put("Result", totalForDate);
                messageDto.setResponse(object);
            } catch (NoResultException e) {
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002") + " - MatterCommunitySection or evaluationPlan not exists");

            }
        } else {
            AssistanceIntegralPk assistanceIntegralPk = new AssistanceIntegralPk();
            assistanceIntegralPk.setSectionId(sectionEntity.getId());
            try {
                List<AssistanceIntegralEntity> assistanceIntegralEntity = assistanceIntegralDao.noAttendanceFindAll(assistanceIntegralPk);
                for (AssistanceIntegralEntity assistanceIntegralEntity1 : assistanceIntegralEntity) {
                    if (!datesClass.contains(assistanceIntegralEntity1.getId().getDate())) {
                        datesClass.add(assistanceIntegralEntity1.getId().getDate());
                    }
                }
                for (int i = 0; i < datesClass.size(); i++) {
                    for (AssistanceIntegralEntity assistanceIntegralEntity1 : assistanceIntegralEntity) {
                        if (datesClass.get(i).equals(assistanceIntegralEntity1.getId().getDate())) {
                            date++;
                            if (!assistanceIntegralEntity1.getAttended().booleanValue()) {
                                noAsistance++;
                            }
                        }
                    }
                    value.put("Date", datesClass.get(i));
                    value.put("Cantidad", date);
                    value.put("inasistencia", noAsistance);
                    date = 0;
                    noAsistance = 0;
                    totalForDate.add(value);
                    value = new JSONObject();
                }

                object.put("Result", totalForDate);
                messageDto.setResponse(object);
            } catch (NoResultException e) {
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002") + " - MatterCommunitySection or evaluationPlan not exists");

            }
        }
        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto qualificationGetStudents(Long matterComunitySectionId, String dateInit, String dateFin){

        MessageDto messageDto = new MessageDto();
        JSONObject value = new JSONObject();
        JSONObject result = new JSONObject();
        List totalForStudent = new ArrayList<>();
        int cantidad = 0;
        int promedio = 0;
        List<QualificationUserDto> qualificationUserDtoList = new ArrayList<QualificationUserDto>();
        try{
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterComunitySectionId);

            for(UserEntity student : matterCommunitySectionEntity.getSection().getStudentList()){
                List<QualificationUserEntity> qualificationUserEntityList = qualificationUserDao.getAllToStudentToDate(matterCommunitySectionEntity.getEvaluationPlan().getId(), student.getId(), dateInit, dateFin);
                if(qualificationUserEntityList.size() == 0){
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("SK-002");
                    messageDto.setMessage("No se encontraron evaluaciones dentro de ese rango");
                    return messageDto;
                }
                for(QualificationUserEntity qualificationUserEntity: qualificationUserEntityList){
                    qualificationUserDtoList.add(entityMapper.entityToBO(qualificationUserEntity));
                    if(qualificationUserEntity.getQualification() != null){
                        if(qualificationUserEntity.getQualification().getValue() != null){
                            cantidad = cantidad + Integer.parseInt(qualificationUserEntity.getQualification().getValue());
                        }else{
                            cantidad = cantidad + 1;
                        }
                    }else{
                        cantidad = cantidad + 1;
                    }
                }
                promedio = cantidad/qualificationUserEntityList.size();
                cantidad = 0;
                value.put("student", student.getFirstName() +" " + student.getLastName());
                value.put("evaluations", qualificationUserDtoList);
                value.put("average",promedio);
                totalForStudent.add(value);
                value = new JSONObject();
                qualificationUserDtoList = new ArrayList<QualificationUserDto>();
            }
            messageDto.setResponse(totalForStudent);
            return messageDto;
            /*List<QualificationUserEntity> qualificationUserEntityList = qualificationUserDao.GetAllToStudent(matterCommunitySectionEntity.getEvaluationPlan().getId(), studentId);

            if(matterCommunitySectionEntity == null || qualificationUserEntityList == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }
            List<QualificationUserDto> qualificationUserDtoList = new ArrayList<QualificationUserDto>();
            for(QualificationUserEntity qualificationUserEntity: qualificationUserEntityList){
                qualificationUserDtoList.add(entityMapper.entityToBO(qualificationUserEntity));
            }

            messageDto.setResponse(qualificationUserDtoList);
            return messageDto;*/

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Search not exit");
            return messageDto;
        }
    }
}
