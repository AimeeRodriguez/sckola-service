package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.QualificationBF;
import com.posma.sckola.app.dto.EvaluationDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.QualificationUserDto;
import com.posma.sckola.app.persistence.dao.EvaluationDao;
import com.posma.sckola.app.persistence.dao.EvaluationPlanDao;
import com.posma.sckola.app.persistence.dao.MatterCommunitySectionDao;
import com.posma.sckola.app.persistence.dao.QualificationUserDao;
import com.posma.sckola.app.persistence.entity.EvaluationEntity;
import com.posma.sckola.app.persistence.entity.MatterCommunitySectionEntity;
import com.posma.sckola.app.persistence.entity.Qualification;
import com.posma.sckola.app.persistence.entity.QualificationUserEntity;
import com.posma.sckola.app.persistence.entity.StatusEvaluation;
import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
@Service
public class QualificationBFImpl implements QualificationBF {

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EvaluationDao evaluationDao;

    @Autowired
    QualificationUserDao qualificationUserDao;

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    EntityMapper entityMapper;

    @Override
    @Transactional
    public MessageDto createQualification(Long matterComunitySectionId,Long evaluationId){

        MessageDto messageDto = new MessageDto();

        try{
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterComunitySectionId);
            EvaluationEntity evaluationEntity = evaluationDao.findOne(evaluationId);

            if(matterCommunitySectionEntity == null || evaluationEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - qualification not exists");
                return messageDto;
            }
            if(matterCommunitySectionEntity.getSection().getStudentList().size() == 0){
                messageDto.setMessage("Students no exists");
                return messageDto;
            }
            List<QualificationUserDto> qualificationUserDtoList = new ArrayList<QualificationUserDto>();
            for(UserEntity userEntity: matterCommunitySectionEntity.getSection().getStudentList()){
                QualificationUserEntity qualificationUserEntityOld = qualificationUserDao.getToStudentEvaluation(evaluationId,userEntity.getId());
                if(qualificationUserEntityOld == null){
                    QualificationUserEntity qualificationUserEntity = new QualificationUserEntity();
                    qualificationUserEntity.setStudent(userEntity);
                    qualificationUserEntity.setEvaluation(evaluationEntity);
                    qualificationUserEntity.setMatter(matterCommunitySectionEntity.getMatterCommunity().getMatter());
                    qualificationUserEntity.setQualification(new Qualification(null,null,null));
                    QualificationUserEntity qualificationUserEntity1 = qualificationUserDao.create(qualificationUserEntity);
                    qualificationUserDtoList.add(entityMapper.entityToBO(qualificationUserEntity1));
                }else{
                    qualificationUserDtoList.add(entityMapper.entityToBO(qualificationUserEntityOld));
                }
            }

            evaluationEntity.setStatus(StatusEvaluation.IN_PROGRESS);
            evaluationDao.update(evaluationEntity);
            EvaluationDto evaluationDto = entityMapper.entityToBO(evaluationEntity);
            evaluationDto.setQualificationUserList(qualificationUserDtoList);

            messageDto.setResponse(evaluationDto);
            messageDto.setSuccess(true);
            messageDto.setMessage(systemMessage.getMessage("SK-000")+ " - Creation Exitosa");
            return messageDto;

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Creation not exists");
            return messageDto;
        }

    }

    @Override
    @Transactional
    public MessageDto updateQualification(Long qualificationId, QualificationUserDto qualificationUserDto){

        MessageDto messageDto = new MessageDto();

        try{
            QualificationUserEntity qualificationUserEntity = qualificationUserDao.findOne(qualificationId);

            if(qualificationUserEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - qualification not exists");
                return messageDto;
            }

            qualificationUserEntity.setQualification(new Qualification(qualificationUserDto.getQualification().getValue(),
                    qualificationUserDto.getQualification().getText(), qualificationUserDto.getQualification().getWeight()));

            qualificationUserDao.update(qualificationUserEntity);

            return messageDto;

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-003");
            messageDto.setMessage(systemMessage.getMessage("SK-003")+ " - Update not exists");
            return messageDto;
        }
    }

    @Override
    @Transactional
    public MessageDto qualificationGetStudent(Long matterComunitySectionId,Long studentId){

        MessageDto messageDto = new MessageDto();

        try{
            MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterComunitySectionId);
            List<QualificationUserEntity> qualificationUserEntityList = qualificationUserDao.GetAllToStudent(matterCommunitySectionEntity.getEvaluationPlan().getId(), studentId);

            if(matterCommunitySectionEntity == null || qualificationUserEntityList == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }
            List<QualificationUserDto> qualificationUserDtoList = new ArrayList<QualificationUserDto>();
            for(QualificationUserEntity qualificationUserEntity: qualificationUserEntityList){
                qualificationUserDtoList.add(entityMapper.entityToBO(qualificationUserEntity));
            }

            messageDto.setResponse(qualificationUserDtoList);
            return messageDto;

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Search not exit");
            return messageDto;
        }
    }

}
