package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.ImageDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.UserDto;
import org.springframework.web.multipart.MultipartFile;


/**
 * Created by Francis Ries on 27/03/2017.
 */
public interface UserBF {

    /**
     * Service to create user
     * @since 27/03/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto userCreate(UserDto userDto);

    /**
     * Service to validate mail account from user
     * @since 27/03/2017
     * @param code validation code
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto userValidate(String code, UserDto userDto);

    /**
     * Service to validate mail account from user
     * @since 10/04/2017
     * @param mail mail that requires recovering password
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto restorePassword(String mail);


    /**
     * Service to update data from user
     * @since 19/04/2017
     * @param userId user identifier
     * @param userDto user dto with all params to update
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto userUpdate(Long userId, UserDto userDto);

    /**
     * Service query all users with role (STUDENT/TEACHER/...) from a community
     * @since 24/04/2017
     * @param roleId role identifier
     * @param communityId community identifier
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getAllUsersByRoleCommunity(Long roleId, Long communityId);

    /**
     * Service query all users with role (STUDENT/TEACHER/...) from a community
     * @since 24/04/2017
     * @param userDto detail user to create
     * @param communityId community identifier
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto userCreateInCommunity(Long communityId, Long roleId, UserDto userDto);

    /**
     * Service query all users with role (STUDENT) from a community and not in Section (sectionId)
     * @since 25/04/2017
     * @param sectionId role identifier
     * @param communityId community identifier
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getAllUsersByRoleCommunityMinusSection(Long sectionId, Long communityId);

    MessageDto getUser(Long userId);

    MessageDto savePhotoUser(Long userId, MultipartFile photo);


    /**
     * Service query user by username (mail)
     * @since 23/05/2017
     * @param userName string used to start session
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto getUser(String userName);

    /**
     * Service to create user facebook
     * @since 28/08/2018
     * @author RicardoBalza
     * @version 1.0
     */
    MessageDto userCreateFacebook(UserDto userDto);

    MessageDto userUpdateFacebook(UserDto userDto);

    MessageDto getUserStudentForMailRepresentative(String mail);
}


