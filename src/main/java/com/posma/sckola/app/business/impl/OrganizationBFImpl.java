package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.OrganizationBF;
import com.posma.sckola.app.dto.OrganizationDto;
import com.posma.sckola.app.persistence.dao.OrganizationDao;
import com.posma.sckola.app.persistence.entity.OrganizationEntity;
import com.posma.sckola.app.util.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Francis on 27/03/2017.
 */
@Service
public class OrganizationBFImpl implements OrganizationBF {


    @Autowired
    OrganizationDao organizationDao;

    @Autowired
    EntityMapper entityMapper;

    /**
     * query a organization
     * @since 27/03/2017
     * @return organization list
     * @author FrancisRies
     * @version 1.0
     */
     @Override
     @Transactional
     public List<OrganizationDto> getAll(){
         List<OrganizationEntity> organizationEntities = organizationDao.findAll();

         List<OrganizationDto> organizationDtoList = new ArrayList<OrganizationDto>();
         for(OrganizationEntity organizationEntity:organizationEntities){
             organizationDtoList.add(entityMapper.entityToBO(organizationEntity));
         }
         return organizationDtoList;
     }


}

