package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.EvaluationDto;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
public interface EvaluationBF {

    MessageDto evaluationGetMatterComunitySection(Long matterComunitySectionId);



    /**
     * Service create a new Evaluation
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto newEvaluation(Long evaluationPlanId, EvaluationDto evaluationDto);


    /**
     * Servicio que permite actualizar una evaluación
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto updateEvaluation(Long evaluationId, EvaluationDto evaluationDto);

    /**
     * Servicio que permite eliminar una evaluación de un plan de evaluaciones
     * @since 27/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto deleteEvaluation (Long evaluationId);

    /**
     * Servicio que permite cambia el estado de la evaluacion a culminado
     * @since 27/04/2017
     * @author Ricardo Balza
     * @version 1.0
     */
    MessageDto culminatedEvaluation (Long evaluationId);

}
