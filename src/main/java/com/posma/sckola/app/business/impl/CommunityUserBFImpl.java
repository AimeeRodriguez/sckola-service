package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.CommunityUserBF;
import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.NotificationMessage;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 26/05/2017.
 */
@Service
public class CommunityUserBFImpl implements CommunityUserBF{

    @Autowired
    CommunityDao communityDao;

    @Autowired
    CommunityUserDao communityUserDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    UserRoleCommunityDao userRoleCommunityDao;

    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    NotificationBF notificationBf;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    NotificationBF notificationBF;

    @Autowired
    NotificationMessage notificationMessage;


    @Override
    @Transactional
    public MessageDto getById(Long communityUserId){
        MessageDto messageDto = new MessageDto();

        try{
            CommunityUserEntity communityUserEntity = communityUserDao.findOne(communityUserId);
            if(communityUserEntity == null || communityUserEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+" - Community no exist");
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(communityUserEntity));

            return messageDto;

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getMessage("GET-COM-USER-001"));
            return messageDto;
        }
    }

    @Override
    @Transactional
    public MessageDto  updateCommunity(Long communityUserId, CommunityDto communityDto){

        MessageDto messageDto = new MessageDto();
        messageDto.setIdRequest(communityUserId);

        CommunityUserEntity communityEntity = communityUserDao.findOne(communityUserId);

        if(communityEntity != null && communityEntity.getId() != null){
            communityEntity.setName(communityDto.getName());
            communityEntity.setDescription(communityDto.getDescription());
            communityEntity = communityUserDao.update(communityEntity);

            if (communityEntity != null) {
                messageDto.setResponse(entityMapper.entityToBO(communityEntity));
                return messageDto;
            } else {
                messageDto.setMessage("No fue posible guardar los cambios en la Base de Datos");
            }
        }else{
            messageDto.setMessage("Id de la comunidad no es valido");
        }

        messageDto.setSuccess(false);
        return messageDto;
    }


    /**
     * Servicio que permite consultar todas las comunidades donde participa un usuario del sistema segun el role (TEACHER/STUDENT)
     * @param userId
     * @param roleId
     * @return List<CommunityDto>
     */
    @Override
    @Transactional
    public MessageDto getByUserIdRoleId(Long userId, Long roleId){

        MessageDto messageDto = new MessageDto();
        List<CommunityDto> communityDtoList = new ArrayList<>();

        try {
            List<UserRoleCommunityEntity> userRoleCommunityList = userRoleCommunityDao.findAllByFieldUserIdRoleId(userId, roleId);

            for (UserRoleCommunityEntity userRoleCommunity : userRoleCommunityList) {
                communityDtoList.add(entityMapper.entityToBO(userRoleCommunity.getCommunityUser()));
            }

            messageDto.setResponse(communityDtoList);

        }catch (Exception e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getErrorCode().get("GET-COM-USER-001"));
        }

        return messageDto;
    }


    /**
     * Servicio que permite AsociarComunidad y Desasociar un usuario del sistema con algun role (TEACHER/STUDENT) a una comunidad.
     * Automaticamente creara una nueva entrada a la tabla COMMUNITY_USER. siendo esta la copia de la comunidad pasada por parametro.
     * @param communityId
     * @param userId
     * @param roleId
     * @return MessageDto
     */
    @Override
    @Transactional
    public MessageDto setCommunityIdUserIdRoleId(Long communityId, Long userId, Long roleId, RequestAssociateDto requestAssociateDto){
        MessageDto messageDto = new MessageDto();
        UserRoleCommunityEntity userRoleCommunityEntity;

        try {
            userRoleCommunityEntity = userRoleCommunityDao.findAllByFieldCommunityIdUserIdRoleId(communityId, userId, roleId);
            if(userRoleCommunityEntity != null && userRoleCommunityEntity.getId() == null){
                userRoleCommunityEntity = null;
            }
        }catch (NoResultException e){
            userRoleCommunityEntity = null;
        }

        // Asociar una comunidad a un usuario
        if(requestAssociateDto.getAssociate()){

            if(userRoleCommunityEntity != null){
                // La asociacion ya existe:
                messageDto.setSuccess(true);
                messageDto.setResponse(entityMapper.entityToBO(userRoleCommunityEntity.getCommunityUser()) );
                return messageDto;
            }

            CommunityEntity communityEntity;
            UserEntity userEntity;
            RoleEntity roleEntity;

            try {
                communityEntity = communityDao.findOne(communityId.longValue());
                userEntity = userDao.findOne(userId.longValue());
                roleEntity = roleDao.findOne(roleId.longValue());

                if(communityEntity == null || userEntity == null || roleEntity == null){
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("POS-COM-USER-002");
                    messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-002"));
                    return messageDto;
                }

            }catch (Exception e){
                e.printStackTrace();
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-002");
                messageDto.setMessage(systemMessage.getMessage("SET-COM-USER-002"));

                return messageDto;
            }

            try{

                CommunityUserEntity communityUserEntity = communityUserDao.create(new CommunityUserEntity(communityEntity));

                UserRoleCommunityEntity userRoleCommunityEntityNew = new UserRoleCommunityEntity(userEntity, roleEntity, communityEntity, communityUserEntity);

                userRoleCommunityEntity = userRoleCommunityDao.create(userRoleCommunityEntityNew);

                if(userRoleCommunityEntity != null && userRoleCommunityEntity.getId() != null) {
                    messageDto.setSuccess(true);
                    messageDto.setResponse(entityMapper.entityToBO(communityUserEntity));
                    //**************Comunidad seleccionada!!//*************
                    //notificationBf.createNotification(userEntity, null, NotificationType.COMMUNITY_SUCCESS, null);

                    //oculta el mensaje de solicitud de comunidad
                    MessageDto notification = notificationBF.getHistoryNotificationByInfo(userEntity.getMail());
                    NotificationHistoryDto historyDto = (NotificationHistoryDto) notification.getResponse();
                    //if(historyDto.getNotifications().size() > 0)
                        //notificationBF.removeActiveNotification(historyDto.getNotifications().get(0).getId());
                    for (DashboardNotificationDto dashboardNotificationDto: historyDto.getNotifications()) {
                        if(dashboardNotificationDto.getFrom().equals(notificationMessage.getCommunity()))
                            notificationBF.removeActiveNotification(dashboardNotificationDto.getId());

                    }


                }else{
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("POS-COM-USER-001");
                    messageDto.setMessage(systemMessage.getMessage("SET-COM-USER-001"));
                }
            }catch (Exception e){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-001");
                messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-001"));
            }

        }else{
            // cuando se va desasociar
            if(userRoleCommunityEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-004");
                messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-004"));
                return messageDto;
            }

            try{
                userRoleCommunityDao.delete(userRoleCommunityEntity);
                messageDto.setSuccess(true);

            }catch (Exception e){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("POS-COM-USER-005");
                messageDto.setMessage(systemMessage.getMessage("POS-COM-USER-005"));
            }
        }

        return messageDto;
    }

}
