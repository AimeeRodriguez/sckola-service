package com.posma.sckola.app.business;


import com.posma.sckola.app.dto.InvitationDto;
import com.posma.sckola.app.dto.MessageDto;

public interface InvitationBF {

    MessageDto invitationCreate(InvitationDto invitationDto);

}
