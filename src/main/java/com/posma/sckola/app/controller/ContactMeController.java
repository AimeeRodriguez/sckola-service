package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.ContactMeBF;
import com.posma.sckola.app.dto.ContactDto;
import com.posma.sckola.app.dto.MessageDto;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Posma-dev on 26/03/2020.
 */
@RestController
@RequestMapping("/skola/contactme")
public class ContactMeController {

    static final Logger logger = Logger.getLogger(ContactMeController.class);

    @Autowired
    ContactMeBF contactMeBF;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> communityCreate(@RequestBody ContactDto contact) throws IOException {

        MessageDto messageDto = contactMeBF.SendContactMe(contact);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }
}
