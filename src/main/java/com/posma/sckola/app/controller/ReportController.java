package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.ReportBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Posma-ricardo on 01/11/2017.
 */
@RestController
@RequestMapping("/skola/report")
public class ReportController {

    static final Logger logger = Logger.getLogger(ReportController.class);

    static final String MessageMatterApproveFail = "No fue posible procesar la solicitud";
    static final String MessageMatterApproveSuccess= "Solicitud procesada";

    @Autowired
    Validation validation;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    ReportBF reportBF;

    @RequestMapping(value="/matterCommunitySection/{matterCommunitySectionId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> reportSectionAverage(@PathVariable("matterCommunitySectionId") Long matterCommunitySectionId) throws IOException {
        MessageDto messageDto;

        if(matterCommunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = reportBF.reportMatterSectionAverage(matterCommunitySectionId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
    @RequestMapping(value = "/noAttendance/{matterCommunitySectionId}", method = RequestMethod.GET)
    public ResponseEntity<?>  getNoAttendance (@PathVariable("matterCommunitySectionId")  Long matterCommunitySectionId) throws IOException{
        MessageDto messageDto;
        if (matterCommunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }
        messageDto = reportBF.reportNoAttendance(matterCommunitySectionId);
        if (messageDto.getSuccess())
            return  ResponseEntity.ok(messageDto);
        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/studentsMatterSection/{matterId}/{sectionId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> reportApprovedMatter(@PathVariable("matterId") Long matterId, @PathVariable("sectionId") Long sectionId) throws IOException {
        MessageDto messageDto;
        if (matterId == null) {
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }
        messageDto = reportBF.studentsMatterSection(sectionId,matterId);

        if (messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/sectionMatterCommunity/{matterId}/{communityId}/{teacherId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> sectionsByMatterCommunity(@PathVariable("matterId") Long matterId, @PathVariable("communityId") Long communityId, @PathVariable("teacherId") Long teacherId) throws IOException {
        MessageDto messageDto;
        if (matterId == null || communityId == null) {
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }
        messageDto = reportBF.sectionsByMatterCommunity(matterId, communityId, teacherId);

        if (messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/matterCommunityTeacher/{communityId}/{teacherId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> matterByCommunityTeacher( @PathVariable("communityId") Long communityId, @PathVariable("teacherId") Long teacherId) throws IOException {
        MessageDto messageDto;
        if (communityId == null || teacherId == null) {
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }
        messageDto = reportBF.matterByCommunityTeacher(communityId, teacherId);

        if (messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * Obtiene el plan de evaluacion de una materia, seccion, comunidad y un profesor
     * @param communityId
     * @param matterId
     * @param sectionId
     * @param userId
     * @return
     * @throws IOException
     */
    @RequestMapping(value="community/{communityId}/matter/{matterId}/section/{sectionId}/user/{userId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> reportPlanEvaluation(@PathVariable("communityId") Long communityId,
                                                  @PathVariable("matterId") Long matterId,
                                                  @PathVariable("sectionId") Long sectionId,
                                                  @PathVariable("userId") Long userId) throws IOException {
        MessageDto messageDto;

        if(communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = reportBF.reportPlanEvaluation(communityId, matterId, sectionId, userId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * Obtener materias de una comunidad
     * @param communityId
     * @return
     * @throws IOException
     */
    @RequestMapping(value="/community/{communityId}/user/{userId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getMatterByCommunityUser(@PathVariable("communityId") Long communityId, @PathVariable("userId") Long userId) throws IOException {
        MessageDto messageDto;

        if(communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = reportBF.getMatterByCommunityUser(communityId, userId);

        if (messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }


    /**
     * Obtiene las secciones de una materia de una comunidad especifica
     * @param matterId
     * @param communityId
     * @return
     */
    @RequestMapping(value = "/matter/{matterId}/community/{communityId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getSectionByMatterCommunity (@PathVariable ("matterId") Long matterId, @PathVariable ("communityId") Long communityId) {
        MessageDto messageDto = new MessageDto();

        if(matterId == null || communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = reportBF.getSectionByMatterCommunity(matterId, communityId);

        if (messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value="/matter_community_section/{matterId}/date_init/{dateInit}/date_fin/{dateFin}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> qualificationGetStudent(@PathVariable("matterId") Long matterComunitySectionId,
                                                     @PathVariable("dateInit") String dateInit,
                                                     @PathVariable("dateFin") String dateFin) throws IOException{

        MessageDto messageDto;

        if(matterComunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = reportBF.qualificationGetStudents(matterComunitySectionId,dateInit,dateFin);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }


}
