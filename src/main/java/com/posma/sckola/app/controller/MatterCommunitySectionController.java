package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.MatterCommunitySectionBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.RequestAssociateDto;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Posma-ricardo on 01/06/2017.
 */
@RestController
@RequestMapping("/skola/matter_community_section")
public class MatterCommunitySectionController {

    static final Logger logger = Logger.getLogger(MatterCommunitySectionController.class);

    static final String MessageMatterApproveFail = "No fue posible procesar la solicitud";
    static final String MessageMatterApproveSuccess= "Solicitud procesada";

    @Autowired
    Validation validation;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    MatterCommunitySectionBF matterCommunitySectionBF;

    @RequestMapping(value="/{matterCommunitySectionId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> matterCommunitySectionDesassociate(@PathVariable("matterCommunitySectionId") Long matterCommunitySectionId,
                                                                @RequestBody RequestAssociateDto requestAssociateDto) throws IOException {

        MessageDto messageDto;

        if(matterCommunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = matterCommunitySectionBF.matterCommunitySectionDesassociate(matterCommunitySectionId,requestAssociateDto);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }
}
