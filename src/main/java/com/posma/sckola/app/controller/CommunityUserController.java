package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.CommunityUserBF;
import com.posma.sckola.app.business.WizardBF;
import com.posma.sckola.app.dto.CommunityDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.RequestAssociateDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Posma-ricardo on 26/05/2017.
 */
@RestController
@RequestMapping("/skola/communityUser")
public class CommunityUserController {

    static final Logger logger = Logger.getLogger(CommunityUserController.class);

    @Autowired
    CommunityUserBF communityUserBF;

    @Autowired
    WizardBF wizardBF;

    @Autowired
    SystemMessage systemMessage;


    /**
     * Servicio que permite consultar todas las comunidades asociadas al usuario del sistema segun el role (TEACHER/STUDENT)
     * @return List CommunityDto
     * @throws IOException
     */
    @RequestMapping(value ="/user/{userId}/role/{roleId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getByUserIdRoleId(@PathVariable("userId") Long userId, @PathVariable("roleId") Long roleId) throws IOException{

        if(userId != null && roleId != null) {
            MessageDto messageDto = communityUserBF.getByUserIdRoleId(userId, roleId);
            if(messageDto != null && messageDto.getSuccess()){
                return ResponseEntity.ok(messageDto);
            }else{
                return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
            }
        }

        MessageDto messageDto = new MessageDto();
        messageDto.setSuccess(false);
        messageDto.setErrorCode("GET-000");
        messageDto.setMessage(systemMessage.getMessage("GET-000"));
        return new ResponseEntity<> (messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Servicio que permite AsociarComunidad y Desasociar un usuario del sistema con algun role (TEACHER/STUDENT) a una comunidad
     * @return List CommunityDto
     * @throws IOException
     */
    @RequestMapping(value ="/community/{communityId}/user/{userId}/role/{roleId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addCommunityIdUserIdRoleId(@PathVariable("communityId") Long communityId,
                                                @PathVariable("userId") Long userId,
                                                @PathVariable("roleId") Long roleId,
                                                        @RequestBody RequestAssociateDto requestAssociateDto) throws IOException{

        if(communityId != null && userId != null && roleId != null && requestAssociateDto != null && requestAssociateDto.getAssociate() != null) {
            MessageDto messageDto = communityUserBF.setCommunityIdUserIdRoleId(communityId, userId, roleId, requestAssociateDto);

            //Gammification
            wizardBF.updateCommunity(communityId,userId);


            if(messageDto != null && messageDto.getSuccess()){
                return ResponseEntity.ok(messageDto);
            }else{
                return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
            }
        }

        MessageDto messageDto = new MessageDto();
        messageDto.setSuccess(false);
        messageDto.setErrorCode("GET-000");
        messageDto.setMessage(systemMessage.getMessage("GET-000"));
        return new ResponseEntity<> (messageDto, HttpStatus.BAD_REQUEST);
    }


    @RequestMapping(value ="/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getById(@PathVariable("id") Long communityId) throws IOException{

            MessageDto messageDto = new MessageDto();
        if(communityId == null){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = communityUserBF.getById(communityId);

        if(messageDto.getSuccess())
        return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> communityUserEdit(@PathVariable("id") Long communityUserId, @RequestBody CommunityDto communityDto) throws IOException{

            MessageDto messageDto = new MessageDto();

        if(communityUserId == null || communityDto == null){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = communityUserBF.updateCommunity(communityUserId, communityDto);
        wizardBF.updateCommunity(communityDto.getId(),communityUserId);

        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
}
