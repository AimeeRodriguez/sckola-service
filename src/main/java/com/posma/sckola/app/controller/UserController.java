package com.posma.sckola.app.controller;


import com.posma.sckola.app.business.SectionBF;
import com.posma.sckola.app.business.UserBF;
import com.posma.sckola.app.business.WizardBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.UserDto;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


/**
 * Created by Francis on 27/03/2017.
 */
@RestController
@RequestMapping("/skola/user")
public class UserController {

    static final Logger logger = Logger.getLogger(UserController.class);

    static final String MessageUserApproveFail = "No fue posible procesar la solicitud";
    static final String MessageUserApproveSuccess= "Solicitud procesada";

    @Autowired
    UserBF userBF;

    @Autowired
    SectionBF sectionBF;

    @Autowired
    WizardBF wizardBF;

    @Autowired
    Validation validation;
    
    @Autowired
    SystemMessage systemMessage;

    /**
     * Servicio que permite crear un Usuario en skola
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> userCreate(@RequestBody UserDto userDto) throws IOException {

        if(userDto == null || userDto.getMail() == null || !validation.isEmail(userDto.getMail()) || userDto.getPassword() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = userBF.userCreate(userDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * Servicio que permite crear un Usuario en una comunidad con un rol particular
     * Utilizado para crear alumnos o cualquier usuario que pertenece a la comunidad pero que no existe en el sistema
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "/role/{roleId}/community/{communityId}",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> userCreateInCommunity(@PathVariable("roleId") Long roleId,
                                                   @PathVariable("communityId") Long communityId,
                                                   @RequestBody UserDto userDto) throws IOException {

        MessageDto messageDto;

        if(userDto == null || userDto.getFirstName() == null || userDto.getFirstName().trim() == ""
                || userDto.getLastName() == null || userDto.getLastName().trim() == ""
                || (userDto.getMail() != null && !validation.isEmail(userDto.getMail()))
                || roleId == null || communityId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        try{
            messageDto = userBF.userCreateInCommunity(communityId, roleId, userDto);
            if(messageDto.getSuccess())
                return ResponseEntity.ok(messageDto);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        messageDto.setSuccess(false);
        messageDto.setErrorCode("SK-001");
        messageDto.setMessage(systemMessage.getMessage("SK-001"));
        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Service query all users with role (STUDENT/TEACHER/...) from a community
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value="/role/{roleId}/community/{communityId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> allUserByRoleCommunity(@PathVariable("roleId") Long roleId,
                                                    @PathVariable("communityId") Long communityId) throws IOException {

        if(roleId == null || communityId == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = userBF.getAllUsersByRoleCommunity(roleId, communityId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Service query all users with role (STUDENT) from a community that not in section X
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value="/student/community/{communityId}/not_section/{sectionId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> allUserByRoleCommunityMinusSection(@PathVariable("sectionId") Long sectionId,
                                                    @PathVariable("communityId") Long communityId) throws IOException {

        if(sectionId == null || communityId == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = userBF.getAllUsersByRoleCommunityMinusSection(sectionId, communityId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Servicio que permite crear un Usuario en skola
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "/validate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> userValidate(@RequestParam(required = true) String code,
                                          @RequestBody UserDto userDto) throws IOException {

        MessageDto messageDto = new MessageDto();

        if(code == null || code.equals("") || userDto == null){
            messageDto.setResponse(code);
            messageDto.setErrorCode("SK-002");             
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = userBF.userValidate(code, userDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);
        return new ResponseEntity<>(messageDto, HttpStatus.NOT_ACCEPTABLE);

    }


    /**
     * Servicio que permite al Usuario recuperar su contraseña
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "/recover", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> recoverPassword(@RequestParam(required = true) String mail) throws IOException {

        MessageDto messageDto = new MessageDto();

        if(mail == null || mail.equals("")){
            messageDto.setResponse(mail);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = userBF.restorePassword(mail);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);
        return new ResponseEntity<>(messageDto, HttpStatus.NOT_ACCEPTABLE);

    }


    /**
     * Servicio que permite actualizar un Usuario en skola
     * este servicio no actualiza ni el password ni el mail, eso se debe realizar por otro servicio
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "/{userId}",method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> userUpdate(@PathVariable("userId") Long userId,
                                        @RequestBody UserDto userDto) throws IOException {

        if(userDto == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = userBF.userUpdate(userId, userDto);
        wizardBF.updateProfileUser(userId);


        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }


    /**
     * Consultar el listado de estudiantes de una seccion
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "/section/{sectionId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUserSection(@PathVariable("sectionId") Long sectionId) throws IOException {

        if(sectionId == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = sectionBF.getAllUserSection(sectionId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Add user to a section
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "{userId}/section/{sectionId}",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addUserSection(@PathVariable("userId") Long userId,
                                        @PathVariable("sectionId") Long sectionId) throws IOException {

        if(sectionId == null && userId == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = sectionBF.associateUserSection(userId, sectionId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * remove user from a section
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "{userId}/section/{sectionId}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> removeUserSection(@PathVariable("userId") Long userId,
                                        @PathVariable("sectionId") Long sectionId) throws IOException {

        if(sectionId == null && userId == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = sectionBF.desassociateUserSection(userId, sectionId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Servicio que permite Buscar un usuario
     * @return List UserDto
     * @throws IOException
     */
    @RequestMapping(value = "/{userId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUser(@PathVariable("userId") Long userId) throws IOException {

        if(userId == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = userBF.getUser(userId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    /**
     * guardar foto al user
     * @return userDto
     * @throws IOException
     */
    @RequestMapping(value = "/{userId}/photo",method = RequestMethod.POST,consumes = { "multipart/mixed", "multipart/form-data" })
    @ResponseBody
    public ResponseEntity<?> savePhoto(@PathVariable("userId") Long userId, @RequestPart("file") MultipartFile file) throws IOException {


        if(userId == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }
        System.out.println(file);

        MessageDto messageDto = userBF.savePhotoUser(userId, file);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/facebook",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> userCreateFacebook(@RequestBody UserDto userDto) throws IOException {

        if(userDto == null || userDto.getMail() == null || !validation.isEmail(userDto.getMail()) || userDto.getPassword() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = userBF.userCreateFacebook(userDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/facebook/update",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> userUpdateFacebook(@RequestBody UserDto userDto) throws IOException {

        if(userDto == null || userDto.getMail() == null || !validation.isEmail(userDto.getMail()) || userDto.getPassword() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = userBF.userUpdateFacebook(userDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
        /**
         * Funcion de manejo de excepciones pertenecientes a la clase Exception
         * @param exc excepcion interceptada
         * @return una entidad respuesta, con el HttpStatus y el mensaje del error
         */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
