package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.InvitationBF;
import com.posma.sckola.app.dto.InvitationDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
@RequestMapping("/skola/invitation")
public class InvitationController {

    static final Logger logger = Logger.getLogger(InvitationController.class);

    @Autowired
    InvitationBF invitationBF;

    @Autowired
    Validation validation;

    @Autowired
    SystemMessage systemMessage;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> userCreate(@RequestBody InvitationDto invitationDto) throws IOException {

        if(invitationDto == null || invitationDto.getMail() == null || !validation.isEmail(invitationDto.getMail())){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = invitationBF.invitationCreate(invitationDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
}
