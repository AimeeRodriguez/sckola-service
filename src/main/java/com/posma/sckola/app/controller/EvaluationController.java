package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.EvaluationBF;
import com.posma.sckola.app.dto.EvaluationDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
@RestController
@RequestMapping("/skola/evaluation")
public class EvaluationController {

    static final Logger logger = Logger.getLogger(EvaluationController.class);

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    EvaluationBF evaluationBF;


    /**
     * Servicio que permite buscar todas las evaluaciones de una materia que esten en el estado PENING y IN_PROGRESS
     * @return Boolean true
     * @throws IOException
     */
    @RequestMapping(value="/matter_community_section/{matterId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> evaluationGetMatterComunitySection(@PathVariable("matterId") Long matterComunitySectionId) throws IOException{
        MessageDto messageDto;

        if(matterComunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = evaluationBF.evaluationGetMatterComunitySection(matterComunitySectionId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }


    /**
     * Servicio que permite crear una nueva evaluación asociandola a un plan existente
     * @return evaluationDto
     * @throws IOException
     */
    @RequestMapping(value = "/evaluation_plan/{planId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> create(@PathVariable("planId") Long planId,
                                             @RequestBody EvaluationDto evaluationDto) throws IOException {

        if(planId == null || evaluationDto == null
                || evaluationDto.getEvaluationTool() == null
                || evaluationDto.getEvaluationTool().getId() == null
                || evaluationDto.getEvaluationScale() == null
                || evaluationDto.getEvaluationScale().getId() == null
                || evaluationDto.getWeight() == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = evaluationBF.newEvaluation(planId, evaluationDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }


    /**
     * Servicio que permite actualizar una evaluación
     * @return evaluationDto
     * @throws IOException
     */
    @RequestMapping(value = "/{evaluationId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> update(@PathVariable("evaluationId") Long evaluationId,
                                             @RequestBody EvaluationDto evaluationDto) throws IOException {

        if(evaluationId == null || evaluationDto == null
                || evaluationDto.getEvaluationTool() == null
                || evaluationDto.getEvaluationTool().getId() == null
                || evaluationDto.getEvaluationScale() == null
                || evaluationDto.getEvaluationScale().getId() == null
                || evaluationDto.getWeight() == null
                || evaluationDto.getEvaluationPlanId() == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = evaluationBF.updateEvaluation(evaluationId, evaluationDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Servicio que permite eliminar una evaluación de un plan de evaluaciones
     * @return evaluationDto
     * @throws IOException
     */
    @RequestMapping(value = "/{evaluationId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable("evaluationId") Long evaluationId) throws IOException {

        if(evaluationId == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = evaluationBF.deleteEvaluation(evaluationId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

    /**
     * Servicio que permite cambia el estado de la evaluacion a culminado
     *
     * @throws IOException
     */
    @RequestMapping(value = "/{evaluationId}/culminated", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> evaluationCulminated(@PathVariable("evaluationId") Long evaluationId) throws IOException {

        if(evaluationId == null){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = evaluationBF.culminatedEvaluation(evaluationId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
    }

}
