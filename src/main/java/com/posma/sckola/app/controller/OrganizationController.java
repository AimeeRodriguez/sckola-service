package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.OrganizationBF;
import com.posma.sckola.app.dto.OrganizationDto;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;


/**
 * Created by Francis on 27/03/2017.
 */
@RestController
@RequestMapping("/skola/organization")
public class OrganizationController {

    static final Logger logger = Logger.getLogger(OrganizationController.class);

    static final String MessageOrganizationResourceNotFound = "Recurso solicitado no encontrado";
    static final String MessageOrganizationApproveFail = "No fue posible procesar la solicitud";
    static final String MessageOrganizationApproveSuccess= "Solicitud procesada";

    @Autowired
    OrganizationBF organizationBF;

    /**
     * Servicio que permite consultar todos los organizations del sistema
     * @return List organizationBO
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<OrganizationDto> getAll() throws IOException {
        return organizationBF.getAll();
    }

    /**
     * Funcion de manejo de excepciones pertenecientes a la clase Exception
     * @param exc excepcion interceptada
     * @return una entidad respuesta, con el HttpStatus y el mensaje del error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }


}
