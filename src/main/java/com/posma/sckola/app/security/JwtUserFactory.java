package com.posma.sckola.app.security;

import com.posma.sckola.app.persistence.entity.RoleEntity;
import com.posma.sckola.app.persistence.entity.StatusUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.posma.sckola.app.persistence.entity.UserEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class JwtUserFactory {
    private JwtUserFactory() {
    }

    public static JwtUser create(UserEntity user) {
        return new JwtUser(
                user.getId(),
                user.getMail(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getRoleList()),
                user.getStatus().equals(StatusUser.ACTIVE)|| user.getStatus().equals(StatusUser.WIZARD),
                //user.getLastPasswordResetDate()
                new Date()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<RoleEntity> authorities) {

        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>();
        for(RoleEntity role: authorities){
            result.add(new SimpleGrantedAuthority(role.getName()));
        }

        return result;
    }
}
