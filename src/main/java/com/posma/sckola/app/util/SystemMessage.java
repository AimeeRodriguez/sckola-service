package com.posma.sckola.app.util;

import com.posma.sckola.app.persistence.dao.SystemMessageDao;
import com.posma.sckola.app.persistence.entity.SystemMessageEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Francis Ries on 18/04/2017.
 */

public class SystemMessage {

    @Autowired
    SystemMessageDao systemMessageDao;

    private Map<String, String> errorCode = new HashMap<>();

    private void load(){
        List<SystemMessageEntity> systemMessageEntities = systemMessageDao.findAll();
        for(SystemMessageEntity systemMessageEntity:systemMessageEntities){
            this.errorCode.put(systemMessageEntity.getErrorCode(),systemMessageEntity.getMessage());
        }
    }

    public void reload(){
        errorCode.clear();
        load();
    }
    /**
     * @return the java.util.Map<java.lang.String,java.lang.String>
     */
    public Map<String, String> getErrorCode() {
        return errorCode;
    }

    public String getMessage(String errorCode) {
        return this.errorCode.get(errorCode);
    }


    /**
     * @param errorCode dupla error code with error message
     */
    public void setErrorCode(Map<String, String> errorCode) {
        this.errorCode = errorCode;
    }

}
