package com.posma.sckola.app.util;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;

/**
 * Created by Francis on 27/03/2017.
 */
@Service
public class Encrypt {

    // Valores posibles para tipo de encriptacion
    public final static String SHA512 = "SHA-512";
    public final static String SHA1 = "SHA-1";
    public final static String MD5 = "MD5";

    public Encrypt(){}

    /**
     * metodo que devuelve el valor hash del texto <code></code> segun el metodo seleccionado
     * @param code tipo de metodo ha aplicar (SHA512, SHA1, MD5)
     * @param textToEncript texto a aplicar hash
     */
    public String getHash(String code, String textToEncript){
        MessageDigest md = null;
        String hash = "";

        try {
            md = MessageDigest.getInstance(code);
            md.update(textToEncript.getBytes());
            byte[] mb = md.digest();
            hash = new String(Hex.encodeHex(mb));
        } catch (NoSuchAlgorithmException e) {
            //Error
            return null;
        }
        return hash;
    }

}