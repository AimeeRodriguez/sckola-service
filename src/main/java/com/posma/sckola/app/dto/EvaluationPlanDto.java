package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 26/04/2017.
 */
@Getter
@Setter
public class EvaluationPlanDto implements Serializable {

    private Long id;

    private String name;

    private Long userId;

    private List<EvaluationDto> evaluations = new ArrayList<EvaluationDto>();

    private MatterCommunitySectionDto matterCommunitySection;

    public EvaluationPlanDto(){}

    public boolean addEvaluations(EvaluationDto evaluation){ return evaluations.add(evaluation);}

    public void setMatterCommunitySection(MatterCommunitySectionDto matterCommunitySection){this.matterCommunitySection = matterCommunitySection;}

}
