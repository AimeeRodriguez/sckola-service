package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Posma-ricardo on 04/05/2017.
 */
@Getter
@Setter
public class SectionTypeDto {

    private String type;

    public SectionTypeDto(){}
}
