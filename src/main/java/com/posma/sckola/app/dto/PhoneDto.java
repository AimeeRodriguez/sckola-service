package com.posma.sckola.app.dto;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Getter
@Setter
public class PhoneDto implements Serializable {

    private Long id;
    private String phone;

    public PhoneDto(){}

    public PhoneDto(String phone){this.phone = phone;}

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'phone':'" + phone + "',\n" +
                "}";
    }
}
