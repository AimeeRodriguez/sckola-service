package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Getter
@Setter
public class MatterCommunitySectionDto implements Serializable {


    private Long id;
    private Long teacherId;
    private MatterCommunityDto matterCommunity;
    private SectionDto section;
    private String type;
    private EvaluationPlanDto evaluationPlan;

    public MatterCommunitySectionDto() {}
}
