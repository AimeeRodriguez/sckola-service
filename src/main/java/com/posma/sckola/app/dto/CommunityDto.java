package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Posma-ricardo on 10/04/2017.
 */
@Getter
@Setter
public class CommunityDto implements Serializable {

    private Long id;

    private String name;

    private String description;

    private CommunityDto communityOrigin;

    public CommunityDto(){}

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'name':'" + name + "',\n" +
                " 'description': '" + description + "',\n" +
                " 'communityOrigin':" + communityOrigin + "',\n" +
                "}";
    }
}
