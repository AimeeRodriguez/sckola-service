package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Getter
@Setter
public class LoginDto implements Serializable {

    private String username;
    private String password;

    public LoginDto() { }

    @Override
    public String toString(){
        return "{ \n" +
                " 'username': '" + username + "',\n" +
                " 'password':' ******* '\n" +
                "}";
    }
}
