package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Francis Ries on 21/04/2017.
 */
@Getter
@Setter
public class SectionDto implements Serializable {

    private Long id;
    private  String name;
    private  Long comunityId;
    private List<UserDto> studentList;
    private Long countStudent;

    //private List<MatterCommunitySectionDto> matterCommunitySectionList = new ArrayList<MatterCommunitySectionDto>();

    public SectionDto() {

    }

    public SectionDto(Long id, String name, Long comunityId) {
        this.id = id;
        this.name = name;
        this.comunityId = comunityId;
    }
}

