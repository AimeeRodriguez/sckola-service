package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Posma-dev on 04/09/2017.
 */
@Getter
@Setter
public class NotificationHistoryDto {
    private List<DashboardNotificationDto> notifications;
    private int activeNotifications;

    public NotificationHistoryDto(List<DashboardNotificationDto> notifications, int activeNotifications) {
        this.notifications = notifications;
        this.activeNotifications = activeNotifications;
    }

    public NotificationHistoryDto() {}
}
