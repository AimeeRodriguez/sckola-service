package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
@Getter
@Setter
public class AssistanceDto implements Serializable {

    private String date;

    private Long matterCommunitySectionId;

    private List<UserAssistDto> userAssistList;

    public AssistanceDto(){}
}
