package com.posma.sckola.app.dto;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 25/04/2017.
 */
@Getter
@Setter
public class EvaluationScaleDto implements Serializable {

    private Long id;
    private String name;
    private String description;
    private List<EvaluationValueDto> evaluationValueList = new ArrayList<EvaluationValueDto>();

    public EvaluationScaleDto() {}

    public EvaluationScaleDto(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public boolean addEvaluationValueList(EvaluationValueDto evaluationValue) {
        return evaluationValueList.add(evaluationValue);
    }


}
