package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
@Getter
@Setter
public class QualificationUserDto implements Serializable {

    private Long id;
    private UserDto user;
    private String nameEvaluation;
    private String dateEvaluation;
    private MatterDto matter;

    private QualificationDto qualification;

    public QualificationUserDto(){}
}
