package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Getter
@Setter
public class LoginResultDto implements Serializable {


    private long userId;
    private UserDto userDetail;
    private String token;
    private List<UserDto> studentId;

    public LoginResultDto() { }
}
