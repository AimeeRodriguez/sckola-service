package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Posma-ricardo on 21/04/2017.
 */
@Getter
@Setter
public class UserAssistDto {

    private Long userId;
    private String firstName;
    private String lastName;
    private Boolean assistance;

    public UserAssistDto(){}

    @Override
    public String toString(){
        return "{ \n" +
                " 'userId':" + userId + ",\n" +
                " 'firstName':" + firstName + ",\n" +
                " 'lastName':" + lastName + ",\n" +
                " 'assistance':'" + assistance + "',\n" +
                "}";
    }
}
