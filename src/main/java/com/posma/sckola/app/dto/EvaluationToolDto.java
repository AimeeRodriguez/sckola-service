package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 25/04/2017.
 */
@Getter
@Setter
public class EvaluationToolDto implements Serializable {

    private Long id;
    private String name;
    private String description;

    public EvaluationToolDto() {}

    public EvaluationToolDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
