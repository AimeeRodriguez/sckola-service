package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
@Getter
@Setter
public class QualificationDto implements Serializable {

    private String value;
    private String text;
    private Integer weight;

    public QualificationDto(){}

    public QualificationDto(String value, String text, Integer weight){
        this.value = value;
        this.text = text;
        this.weight = weight;
    }
}
