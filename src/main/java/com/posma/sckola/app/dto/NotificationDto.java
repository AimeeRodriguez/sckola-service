package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 16/02/2017.
 */
@Getter
@Setter
public class NotificationDto implements Serializable {

    private String to;
    private String title;
    private String text;

    public NotificationDto() { }

    @Override
    public String toString(){
        return "{ \n" +
                " 'to':" + to + ",\n" +
                " 'title':" + title + ",\n" +
                " 'text':'" + text + "',\n" +
                "}";
    }
}
