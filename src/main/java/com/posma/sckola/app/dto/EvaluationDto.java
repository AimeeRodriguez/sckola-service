package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Posma-ricardo on 25/04/2017.
 */
@Getter
@Setter
public class EvaluationDto implements Serializable {

    private Long id;

    private String objective;

    private Integer weight;

    private EvaluationToolDto evaluationTool;

    private EvaluationScaleDto evaluationScale;

    private String date;

    private String status;

    private Long evaluationPlanId;

    private List<QualificationUserDto> qualificationUserList;


    public EvaluationDto(){}
}