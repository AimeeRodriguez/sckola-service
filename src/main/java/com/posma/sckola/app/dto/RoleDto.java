package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 24/04/2017.
 */
@Getter
@Setter
public class RoleDto implements Serializable {

    private Long id;

    private String name;

    private String description;


    public RoleDto() {}

    public RoleDto(String name) {
        this.name = name;
    }
}
