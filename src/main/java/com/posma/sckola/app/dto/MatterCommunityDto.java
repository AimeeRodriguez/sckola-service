package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Getter
@Setter
public class MatterCommunityDto implements Serializable {

    private Long id;
    private String name;
    private String code;
    private Long matterId;
    private Long communityId;
    private String communityName;
    private String description;
    private String objective;
    private String especificObjective;

    public MatterCommunityDto() {

    }

    public MatterCommunityDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
