package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Posma-dev on 26/03/2020.
 */
@Getter
@Setter
public class ContactDto implements Serializable {

    private String name;

    private String message;

    private String email;

    public ContactDto(){}
}
