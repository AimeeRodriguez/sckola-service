package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Getter
@Setter
public class OrganizationDto implements Serializable {

    private Long id;
    private String taxIdentity;
    private String businessName;
    private String email;
    private String address;
    private List<PhoneDto> phoneList = new ArrayList<PhoneDto>();

    public OrganizationDto() { }
    public boolean addPhoneList(PhoneDto phoneDto) {
        return phoneList.add(phoneDto);
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'taxIdentity':'" + taxIdentity + "',\n" +
                " 'businessName':'" + businessName + "',\n" +
                " 'email':'" + email + "',\n" +
                " 'address':'" + address + "',\n" +
                " 'users': " + phoneList.toString() + " \n" +
                "}";
    }

}
