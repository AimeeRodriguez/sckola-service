package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 25/04/2017.
 */
@Getter
@Setter
public class EvaluationValueDto implements Serializable {

    private Long id;

    private String value;

    private String text;

    private Integer weight;


    public EvaluationValueDto() {

    }

    public EvaluationValueDto(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public EvaluationValueDto(String value, String text, Integer weight) {
        this.value = value;
        this.text = text;
        this.weight = weight;
    }
}
