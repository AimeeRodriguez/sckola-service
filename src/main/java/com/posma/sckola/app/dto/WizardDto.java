package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 29/05/2017.
 */
@Getter
@Setter
public class WizardDto implements Serializable {

    private Long userId;
    private Long profileUser;
    private String stepName;
    private CommunityDto community;
    private MatterCommunitySectionDto matterCommunitySection;
    private EvaluationPlanDto evaluationPlan;

    public WizardDto() { }

    @Override
    public String toString(){
        return "{ \n" +
                " 'userId': " + userId + ",\n" +
                " 'stepName':'" + stepName +" \n" +
                " 'community': " + community != null?community.toString():"null" +" \n" +
                " 'matterCommunitySection': " + matterCommunitySection != null?matterCommunitySection.toString():"null" +" \n" +
                " 'evaluationPlan': "+ evaluationPlan.toString() +" \n" +
                "}";
    }
}
