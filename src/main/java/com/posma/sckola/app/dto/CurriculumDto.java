package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Getter
@Setter
public class CurriculumDto implements Serializable {

    private Long id;

    private Long userId;

    private String title;

    private CommunityDto community;

    private String date;

    private String institute;

    public CurriculumDto() {

    }

    public CurriculumDto(Long userId, String title, CommunityDto community, String date) {
        this.userId = userId;
        this.title = title;
        this.community = community;
        this.date = date;
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'userId':" + userId + ",\n" +
                " 'title':'" + title + "',\n" +
                " 'institute':'" + institute + "',\n" +
                " 'community':" + community != null?community.toString():"" + ",\n" +
                " 'date':'" + date + "'\n" +
                "}";
    }

}
