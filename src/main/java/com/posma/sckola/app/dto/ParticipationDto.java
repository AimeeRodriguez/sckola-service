package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ParticipationDto implements Serializable {

    private Long id;
    private Long invitationId;
    private String name;

    public ParticipationDto(){}
}
