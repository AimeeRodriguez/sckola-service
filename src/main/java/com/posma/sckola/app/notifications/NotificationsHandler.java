package com.posma.sckola.app.notifications;

import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.dto.DashboardNotificationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Posma-dev on 17/08/2017.
 */
@Component
@EnableScheduling
public class NotificationsHandler {

    private static final Logger log = Logger.getLogger("logger");

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private NotificationBF notificationBF;


    @Scheduled(fixedDelay = 30000)
    public void sendStuff() {

        log.info("Sending data! " + System.currentTimeMillis());
        //messagingTemplate.convertAndSend("/topic/notify", "Public: " + System.currentTimeMillis());
        List<DashboardNotificationDto> notifications = notificationBF.getNotifications();
        for(DashboardNotificationDto notification: notifications){
            template.convertAndSendToUser(notification.getUsername(), "/notify", notification);
            notificationBF.updateNotificationStatus(notification);
            template.convertAndSendToUser("notYou", "/notify", "Mr Developer Should Not See This: " + System.currentTimeMillis());
        }

    }

}