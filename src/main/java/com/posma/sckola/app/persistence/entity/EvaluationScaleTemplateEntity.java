package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
@Entity
@Table(name = "EVALUATION_SCALE_TEMPLATE")
public class EvaluationScaleTemplateEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_SCALE_TEMPLATE_SEQ", sequenceName = "S_ID_EVALUATION_SCALE_TEMPLATE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_SCALE_TEMPLATE_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false, length = 50)
    private String name;

    @Column(name = "DESCRIPTION", nullable=true, length = 512)
    private String description;

    @OneToMany(mappedBy="evaluationScaleTemplate", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<EvaluationValueTemplateEntity> evaluationValueList = new ArrayList<EvaluationValueTemplateEntity>();

    public EvaluationScaleTemplateEntity() {

    }

    public EvaluationScaleTemplateEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EvaluationValueTemplateEntity> getEvaluationValueList() {
        return evaluationValueList;
    }

    public void setEvaluationValueList(List<EvaluationValueTemplateEntity> evaluationValueList) {
        this.evaluationValueList = evaluationValueList;
    }

    public boolean addEvaluationValueList(EvaluationValueTemplateEntity evaluationValueTemplateEntity) {
        return evaluationValueList.add(evaluationValueTemplateEntity);
    }


}
