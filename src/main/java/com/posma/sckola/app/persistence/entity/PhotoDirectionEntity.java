package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Posma-ricardo on 13/07/2017.
 */
@Entity
@Table(name = "PHOTO_DIRECTION")
public class PhotoDirectionEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "PHOTO_DIRECTION_SEQ", sequenceName = "S_ID_PHOTO_DIRECTION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PHOTO_DIRECTION_SEQ")
    private Long id;

    @Column(name = "DIRECCION")
    private String direccion;

    public PhotoDirectionEntity(){}

    public PhotoDirectionEntity(String direccion){
        this.direccion = direccion;
    }

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public String getDireccion() {return direccion;}

    public void setDireccion(String direccion){this.direccion = direccion;}
}
