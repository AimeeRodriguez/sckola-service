package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Entity
@Table(name = "ASSISTANCE_BY_MATTER",
        indexes = {
                @Index(columnList ="USER_ID",name = "IDX_USER_ID_ASSISTANCE_BY_MATTER"),
                @Index(columnList ="MATTER_COMMUNITY_SECTION_ID",name = "IDX_MAT_COMM_SECT_ID_ASSISTANCE_BY_MATTER"),
                @Index(columnList ="DATE",name = "IDX_DATE_ASSISTANCE_BY_MATTER")
        })
public class AssistanceByMatterEntity implements Serializable {

    @Id
    @Embedded
    private AssistanceByMatterPk id;

    @Column(name = "ATTENDED", nullable=false)
    private Boolean attended;

    // no es un campo com tal, solo crea relacion (contraint) de Foreign key
    @ManyToOne
    @JoinColumn(name = "USER_ID", insertable = false, updatable = false)
    private UserEntity user;

    // no es un campo com tal, solo crea relacion (contraint) de Foreign key
    @ManyToOne
    @JoinColumn(name = "MATTER_COMMUNITY_SECTION_ID", insertable = false, updatable = false)
    private MatterCommunitySectionEntity matterCommunitySection;

    public AssistanceByMatterEntity() {

    }

    public AssistanceByMatterEntity(AssistanceByMatterPk id, Boolean attended) {
        this.id = id;
        this.attended = attended;
    }


    public AssistanceByMatterPk getId() {
        return id;
    }

    public void setId(AssistanceByMatterPk id) {
        this.id = id;
    }

    public Boolean getAttended() {
        return attended;
    }

    public void setAttended(Boolean attended) {
        this.attended = attended;
    }

}



