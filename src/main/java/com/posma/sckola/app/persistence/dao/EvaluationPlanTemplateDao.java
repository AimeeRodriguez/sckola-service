package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationPlanTemplateEntity;
import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
public interface EvaluationPlanTemplateDao {

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @return EvaluationPlanTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationPlanTemplateEntity findOne(long id);

    /**
     * description
     * @since 27/04/2017
     * @return List<EvaluationPlanTemplateEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationPlanTemplateEntity> findAll();


    /**
     * description
     * @since 27/04/2017
     * @return List<EvaluationPlanTemplateEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationPlanTemplateEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description
     * @since 27/04/2017
     * @param evaluationPlanTemplate
     * @return EvaluationPlanTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationPlanTemplateEntity create(EvaluationPlanTemplateEntity evaluationPlanTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationPlanTemplate
     * @return EvaluationPlanTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationPlanTemplateEntity update(EvaluationPlanTemplateEntity evaluationPlanTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationPlanTemplate
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationPlanTemplateEntity evaluationPlanTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

}
