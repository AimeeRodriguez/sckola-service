package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.SystemMessageEntity;

import java.util.List;

/**
 * Created by Francis Ries on 18/04/2017.
 */
public interface SystemMessageDao {

    /**
     * description
     * @since 18/04/2017
     * @param id systemMessage identifier
     * @return SystemMessageEntity
     * @author PosmaGroup
     * @version 1.0
     */
    SystemMessageEntity findOne(long id);

    /**
     * description
     * @since 18/04/2017
     * @return List<SystemMessageEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<SystemMessageEntity> findAll();

    /**
     * description
     * @since 18/04/2017
     * @param systemMessage
     * @return SystemMessageEntity
     * @author PosmaGroup
     * @version 1.0
     */
    SystemMessageEntity create(SystemMessageEntity systemMessage);

    /**
     * description
     * @since 18/04/2017
     * @param systemMessage
     * @return SystemMessageEntity
     * @author PosmaGroup
     * @version 1.0
     */
    SystemMessageEntity update(SystemMessageEntity systemMessage);

    /**
     * description
     * @since 18/04/2017
     * @param systemMessage
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(SystemMessageEntity systemMessage);

    /**
     * description
     * @since 18/04/2017
     * @param id systemMessage identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

}
