package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.RatingScaleDao;
import com.posma.sckola.app.persistence.entity.RatingScaleEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("RatingScaleDao")
public class RatingScaleDaoImpl extends AbstractDao<RatingScaleEntity> implements RatingScaleDao {

    public RatingScaleDaoImpl(){
        super ();
        setClazz(RatingScaleEntity.class);
    }


}
