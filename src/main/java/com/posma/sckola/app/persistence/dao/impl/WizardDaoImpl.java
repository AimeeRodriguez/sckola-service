package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.WizardDao;
import com.posma.sckola.app.persistence.entity.WizardEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 29/05/2017.
 */
@Repository("WizardDao")
public class WizardDaoImpl extends AbstractDao<WizardEntity> implements WizardDao {

    public WizardDaoImpl(){
        super ();
        setClazz(WizardEntity.class);
    }

}
