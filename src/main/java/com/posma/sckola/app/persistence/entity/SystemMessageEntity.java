package com.posma.sckola.app.persistence.entity;

import org.springframework.http.HttpStatus;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Entity
@Table(name = "SYSTEM_MESSAGE",
        indexes = {
                @Index(columnList ="ERROR_CODE",name = "IDX_ERROR_CODE_SYSTEM_MESSAGE")
        })
public class
        SystemMessageEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "SYSTEM_MESSAGE_SEQ", sequenceName = "S_ID_SYSTEM_MESSAGE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYSTEM_MESSAGE_SEQ")
    private Long id;

    @Column(name = "ERROR_CODE", nullable=false)
    private String errorCode;

    @Column(name = "MESSAGE", nullable=false, length = 512)
    private String message;

    @Column(name = "HTTP_CODE", nullable=true,columnDefinition = "int default '23'")
    private HttpStatus httpCode;
    

    public SystemMessageEntity() {

    }

    public SystemMessageEntity(String errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public SystemMessageEntity(String errorCode, String message, HttpStatus httpCode) {
        this.errorCode = errorCode;
        this.message = message;
        this.httpCode = httpCode;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(HttpStatus httpCode) {
        this.httpCode = httpCode;
    }

}
