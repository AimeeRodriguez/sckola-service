package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.MatterCommunitySectionEntity;
import com.posma.sckola.app.persistence.entity.SectionType;
import com.posma.sckola.app.persistence.entity.StatusClass;

import java.util.List;

/**
 * Created by Francis Ries on 21/04/2017.
 */
public interface MatterCommunitySectionDao {

    /**
     * description
     * @since 21/04/2017
     * @param id evaluation identifier
     * @return MatterCommunitySectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterCommunitySectionEntity findOne(long id);

    /**
     * description
     * @since 21/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunitySectionEntity> findAll();

    /**
     * description
     * @since 21/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunitySectionEntity> findAllByFieldId(String fieldName, long id);

    /**
     * description
     * @since 21/04/2017
     * @param matterCommunitySection
     * @return MatterCommunitySectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterCommunitySectionEntity create(MatterCommunitySectionEntity matterCommunitySection);

    /**
     * description
     * @since 21/04/2017
     * @param matterCommunitySection
     * @return MatterCommunitySectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterCommunitySectionEntity update(MatterCommunitySectionEntity matterCommunitySection);

    /**
     * description
     * @since 21/04/2017
     * @param matterCommunitySection
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(MatterCommunitySectionEntity matterCommunitySection);

    /**
     * description
     * @since 21/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);


    /**
     * description
     * @since 21/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacher(long communityId, long userId, StatusClass status);


    /**
     * description consultar todas las materias activas de un profesor, en todas las comunidades
     * @since 21/04/2017
     * @return List<MatterCommunitySectionEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacher(long userId, StatusClass status);


    /**
     * description
     * @since 21/04/2017
     * @return MatterCommunitySectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterCommunitySectionEntity findMatterCommunitySectionUser(long matterCommunityId, long sectionId, long userId, StatusClass status);

    /**
     * description
     * @since 05/05/2017
     * @return MatterCommunitySectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacherWithoutEvaluationPlan(long communityId, long userId, StatusClass status);

    /**
     * description
     * @since 10/05/2017
     * @return MatterCommunitySectionEntity
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterCommunitySectionEntity> findAllMatterCommunitySectionByTeacherByType(long communityId, long userId, StatusClass status, SectionType sectionType);
    
    
}
