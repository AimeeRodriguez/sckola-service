package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.MatterEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface MatterDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return MatterEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<MatterEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @return List<MatterEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<MatterEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description
     * @since 12/04/2017
     * @param matter
     * @return MatterEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterEntity create(MatterEntity matter);

    /**
     * description
     * @since 12/04/2017
     * @param matter
     * @return MatterEntity
     * @author PosmaGroup
     * @version 1.0
     */
    MatterEntity update(MatterEntity matter);

    /**
     * description
     * @since 12/04/2017
     * @param matter
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(MatterEntity matter);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
