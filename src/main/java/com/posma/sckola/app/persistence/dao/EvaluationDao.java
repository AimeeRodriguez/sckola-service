package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface EvaluationDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return EvaluationEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<EvaluationEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param evaluation
     * @return EvaluationEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationEntity create(EvaluationEntity evaluation);

    /**
     * description
     * @since 12/04/2017
     * @param evaluation
     * @return EvaluationEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationEntity update(EvaluationEntity evaluation);

    /**
     * description
     * @since 12/04/2017
     * @param evaluation
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationEntity evaluation);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

    List<EvaluationEntity> getAllEvaluationPlan(Long evaluationPlanId);
}
