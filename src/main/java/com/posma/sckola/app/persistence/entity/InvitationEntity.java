package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "INVITATION",
        indexes = {
                @Index(columnList ="ID",name = "IDX_ID_INVITATION")
        })
public class InvitationEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "INVITATION_SEQ", sequenceName = "S_ID_INVITATION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INVITATION_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=true, length = 80)
    private String name;

    @Column(name = "MAIL", nullable=true, unique = true, length = 255)
    private String mail;

    @Column(name = "PHONE", nullable=true, length = 80)
    private String phone;

    @Column(name = "OCUPATION", nullable=true, length = 80)
    private String ocupation;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="PARTICIPATION_ID", referencedColumnName="ID")
    private List<ParticipationEntity> participation;

    @Column(name = "INSTITUTION", nullable=true, length = 255)
    private String institution;

    @Column(name = "OTHER", nullable=true, length = 255)
    private String other;

    public InvitationEntity(){}

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public String getMail(){return mail;}

    public void setMail(String mail){this.mail = mail;}

    public String getPhone(){return phone;}

    public void setPhone(String phone){this.phone = phone;}

    public String getOcupation(){return ocupation;}

    public void setOcupation(String ocupation){this.ocupation = ocupation;}

    public List<ParticipationEntity> getParticipation(){return participation;}

    public void setParticipation(List<ParticipationEntity> participation){this.participation = participation;}

    public String getInstitution(){return institution;}

    public void setInstitution(String institution){this.institution = institution;}

    public String getOther(){return other;}

    public void setOther(String other){this.other = other;}

}
