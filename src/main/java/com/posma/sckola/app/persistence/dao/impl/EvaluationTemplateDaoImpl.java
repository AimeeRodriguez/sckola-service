package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationTemplateDao;
import com.posma.sckola.app.persistence.entity.EvaluationTemplateEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 27/04/2017.
 */

@Repository("EvaluationTemplateDao")
public class EvaluationTemplateDaoImpl extends AbstractDao<EvaluationTemplateEntity> implements EvaluationTemplateDao {

    public EvaluationTemplateDaoImpl(){
        super ();
        setClazz(EvaluationTemplateEntity.class);
    }

}
