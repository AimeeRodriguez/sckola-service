package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.MatterEntity;
import com.posma.sckola.app.persistence.entity.SectionEntity;
import com.posma.sckola.app.persistence.entity.UserEntity;

import java.util.List;

/**
 * Created by PosmaGroup on 07/11/2017.
 */
public interface ReportDao {

    public List<UserEntity> studentsMatterSection(Long sectionId, Long matterId);

    public List<SectionEntity> sectionsByMatterCommunity(Long matterId, Long communityId, Long teacherId);

    public List<MatterEntity> matterByCommunityTeacher(Long communityId, Long teacherId);
}
