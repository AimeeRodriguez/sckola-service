package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.AssistanceByMatterEntity;
import com.posma.sckola.app.persistence.entity.AssistanceByMatterPk;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface AssistanceByMatterDao {

    /**
     * description
     * @since 12/04/2017
     * @param id assistance identifier
     * @return AssistanceByMatterEntity
     * @author PosmaGroup
     * @version 1.0
     */
    AssistanceByMatterEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<AssistanceByMatterEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<AssistanceByMatterEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param assistance
     * @return AssistanceByMatterEntity
     * @author PosmaGroup
     * @version 1.0
     */
    AssistanceByMatterEntity create(AssistanceByMatterEntity assistance);

    /**
     * description
     * @since 12/04/2017
     * @param assistance
     * @return AssistanceByMatterEntity
     * @author PosmaGroup
     * @version 1.0
     */
    AssistanceByMatterEntity update(AssistanceByMatterEntity assistance);

    /**
     * description
     * @since 12/04/2017
     * @param assistance
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(AssistanceByMatterEntity assistance);

    /**
     * description
     * @since 12/04/2017
     * @param id assistance identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

    AssistanceByMatterEntity findOneAssistance(AssistanceByMatterPk assistanceByMatterPk);

    List<AssistanceByMatterEntity> findAllForSectionForDate(AssistanceByMatterPk assistanceByMatterPk);

    List<AssistanceByMatterEntity> noAttendanceFindAllNonIntegral(AssistanceByMatterPk assistanceByMatterPk);
}
