package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.ReportDao;
import com.posma.sckola.app.persistence.entity.MatterEntity;
import com.posma.sckola.app.persistence.entity.SectionEntity;
import com.posma.sckola.app.persistence.entity.UserEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by PosmaGroup on 07/11/2017.
 */
@Repository("ReportDao")
public class ReportDaoImpl implements ReportDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings("unchecked")
    public List<UserEntity> studentsMatterSection(Long sectionId, Long matterId) {

        return entityManager.createQuery("select mcs.section.studentList from MatterCommunitySectionEntity mcs where mcs.section.id = :sectionId and mcs.matterCommunity.matter.id = :matterId")
                .setParameter("sectionId", sectionId)
                .setParameter("matterId",matterId)
                .getResultList();

    }
    @Override
    @SuppressWarnings("unchecked")
    public List<SectionEntity> sectionsByMatterCommunity(Long matterId, Long communityId, Long teacherId) {
       return entityManager.createQuery("select mcs.section from MatterCommunitySectionEntity mcs where mcs.matterCommunity.matter.id = :matterId and mcs.matterCommunity.community.id = :communityId and mcs.user.id = :teacherId" )
                .setParameter("matterId", matterId)
                .setParameter("communityId", communityId)
                .setParameter("teacherId", teacherId)
                .getResultList();
    }
    @Override
    @SuppressWarnings("unchecked")
    public List<MatterEntity> matterByCommunityTeacher(Long communityId, Long teacherId) {
        return entityManager.createQuery("select mcs.matterCommunity.matter from MatterCommunitySectionEntity mcs where mcs.matterCommunity.community.id = :communityId and mcs.user.id = :teacherId")
                .setParameter("communityId", communityId)
                .setParameter("teacherId",teacherId)
                .getResultList();

    }

}
