package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationPlanEntity;
import com.posma.sckola.app.persistence.entity.Status;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface EvaluationPlanDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return EvaluationPlanEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationPlanEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationPlanEntity> findAll();


    /**
     * description
     * @since 12/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationPlanEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description
     * @since 12/04/2017
     * @param evaluationPlan
     * @return EvaluationPlanEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationPlanEntity create(EvaluationPlanEntity evaluationPlan);

    /**
     * description
     * @since 12/04/2017
     * @param evaluationPlan
     * @return EvaluationPlanEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationPlanEntity update(EvaluationPlanEntity evaluationPlan);

    /**
     * description
     * @since 12/04/2017
     * @param evaluationPlan
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationPlanEntity evaluationPlan);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

    /**
     * description
     * @since 26/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationPlanEntity> findAllByTeacherCommunity(Long communityId, Long userId, Status status);


    /**
     * description
     * @since 26/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationPlanEntity> findAllByTeacher(Long userId, Status status);


    /**
     * description
     * @since 26/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationPlanEntity> findAllByTeacherNotAssociate(Long userId);

    List<EvaluationPlanEntity> findByMatterSectionCommunity(Long p_community, Long p_matter, Long p_section, Long p_userId);

}
