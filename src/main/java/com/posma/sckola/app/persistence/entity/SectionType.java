package com.posma.sckola.app.persistence.entity;


/**
 * Created by Francis on 27/03/2017.
 *
 * INTEGRAL: una seccion integral es aquella donde el manejo de asistencia se hace global sin importar las
 *      diferentes materias vistas en el dia. ejemplo basica de 1-6
 * NON_INTEGRAL: una seccion  No integral es aquella donde el manejo de asistencia se hace por materias sin importar.
 *      es decir, cada materia dada en el mismo seccion (clase) manejara una asistencia independiente. ejemplo Liceo de 7-9 4-5 Universidad
 */

public enum SectionType {
    INTEGRAL, NON_INTEGRAL
}