package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Francis Ries on 26/05/2017.
 */
@Entity
@Table(name = "COMMUNITY_USER",
        indexes = {
                @Index(columnList ="NAME",name = "IDX_NAME_COMMUNITY_USER")
        })
public class CommunityUserEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name="COMMUNITY_USER_SEQ", sequenceName="S_ID_COMMUNITY_USER", initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMUNITY_USER_SEQ")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(mappedBy="communityUser", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserRoleCommunityEntity> userRoleCommunityList;

    /*
    @OneToMany(mappedBy="community", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<StudentEntity> studentList;
*/

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name="COMMUNITY_ID")
    private CommunityEntity community;


    public CommunityUserEntity(){}

    public CommunityUserEntity(CommunityEntity community){
        this.community = community;
        this.name = community.getName();
        this.description = community.getDescription();
    }

    public CommunityUserEntity(CommunityEntity community, String name, String description){
        this.community = community;
        this.name = name;
        this.description = description;
    }

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public String getDescription(){return description;}

    public void setDescription(String description){this.description = description;}

    public List<UserRoleCommunityEntity> getUserRoleCommunityList() {
        return userRoleCommunityList;
    }

    public void setUserRoleCommunityList(List<UserRoleCommunityEntity> userRoleCommunityList) {
        this.userRoleCommunityList = userRoleCommunityList;
    }

    public boolean addUserRoleCommunityList(UserRoleCommunityEntity userRoleCommunity) {
        return userRoleCommunityList.add(userRoleCommunity);
    }
/*
    public List<StudentEntity> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<StudentEntity> studentList) {
        this.studentList = studentList;
    }

    public boolean addStudentList(StudentEntity student) {
        return studentList.add(student);
    }
*/
    public CommunityEntity getCommunity() {
        return community;
    }

    public void setCommunity(CommunityEntity community) {
        this.community = community;
    }


}
