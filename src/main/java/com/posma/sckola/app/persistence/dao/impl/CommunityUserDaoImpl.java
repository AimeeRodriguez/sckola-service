package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.CommunityUserDao;
import com.posma.sckola.app.persistence.entity.CommunityUserEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis Ries on 26/05/2017.
 */
@Repository("CommunityUserDao")
public class CommunityUserDaoImpl extends AbstractDao<CommunityUserEntity> implements CommunityUserDao {

    public CommunityUserDaoImpl(){
        super();
        setClazz(CommunityUserEntity.class);
    }
}
