package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.CommunityUserEntity;

import java.util.List;

/**
 * Created by Francis Ries on 26/05/2017.
 */
public interface CommunityUserDao {

    /**
     * description
     * @since 26/05/2017
     * @param id community identifier
     * @return CommunityUserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CommunityUserEntity findOne(long id);

    /**
     * description
     * @since 26/05/2017
     * @return List<CommunityUserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<CommunityUserEntity> findAll();

    /**
     * description
     * @since 26/05/2017
     * @param community
     * @return CommunityUserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CommunityUserEntity create(CommunityUserEntity community);

    /**
     * description
     * @since 26/05/2017
     * @param community
     * @return CommunityUserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CommunityUserEntity update(CommunityUserEntity community);

    /**
     * description
     * @since 26/05/2017
     * @param community
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(CommunityUserEntity community);

    /**
     * description
     * @since 26/05/2017
     * @param id community identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
