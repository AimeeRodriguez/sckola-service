package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.AssistanceByMatterDao;
import com.posma.sckola.app.persistence.entity.AssistanceByMatterEntity;
import com.posma.sckola.app.persistence.entity.AssistanceByMatterPk;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("AssistanceByMatterDao")
public class AssistanceByMatterDaoImpl extends AbstractDao<AssistanceByMatterEntity> implements AssistanceByMatterDao {

    public AssistanceByMatterDaoImpl(){
        super ();
        setClazz(AssistanceByMatterEntity.class);
    }

    public AssistanceByMatterEntity findOneAssistance(AssistanceByMatterPk assistanceByMatterPk){
        List<AssistanceByMatterEntity> assistanceByMatterEntityList = this.getEntityManager().createQuery("SELECT m FROM AssistanceByMatterEntity as m where m.id.user ='" + assistanceByMatterPk.getUser() + "' and m.id.matterCommunitySection ='" + assistanceByMatterPk.getMatterCommunitySection() + "' and m.id.date ='" + assistanceByMatterPk.getDate() + "'", AssistanceByMatterEntity.class).getResultList();
        return assistanceByMatterEntityList.size() == 1 ? assistanceByMatterEntityList.get(0):null;
    }

    public List<AssistanceByMatterEntity> findAllForSectionForDate(AssistanceByMatterPk assistanceByMatterPk){
        Query query = this.getEntityManager().createQuery("SELECT m FROM AssistanceByMatterEntity as m where m.id.matterCommunitySection =:matterCommunitySection and m.id.date =:date");
        query.setParameter("matterCommunitySection", assistanceByMatterPk.getMatterCommunitySection());
        query.setParameter("date", assistanceByMatterPk.getDate());
        return query.getResultList();
    }

    public List<AssistanceByMatterEntity> noAttendanceFindAllNonIntegral(AssistanceByMatterPk assistanceByMatterPk){
        Query query = this.getEntityManager().createQuery("SELECT m FROM AssistanceByMatterEntity as m where m.id.matterCommunitySection =:matterCommunitySection");
        query.setParameter("matterCommunitySection", assistanceByMatterPk.getMatterCommunitySection());
        return query.getResultList();
    }

}
