package com.posma.sckola.app.persistence.entity;

/**
 * Created by Posma-ricardo on 25/04/2017.
 *
 * PENDING: evaluacion pendiente
 * IN_PROGRESS: evaluacion en progreso
 * CULMINATED: evaluacion culminada
 */
public enum  StatusEvaluation {
    PENDING,IN_PROGRESS,CULMINATED
}
