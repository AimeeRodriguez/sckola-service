package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 12/04/2017.
 */
@Entity
@Table(name = "QUALIFICATION",
        indexes = {
                @Index(columnList ="USER_ID",name = "IDX_STUDENT_QUALIFICATION"),
                @Index(columnList ="MATTER_ID",name = "IDX_MATTER_QUALIFICATION"),
                @Index(columnList ="EVALUATION_ID",name = "IDX_EVALUATION_QUALIFICATION")
        })
public class QualificationUserEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "QUALIFICATION_SEQ", sequenceName = "S_ID_QUALIFICATION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUALIFICATION_SEQ")
    private Long id;

    @ManyToOne
    @JoinColumn(name="USER_ID", referencedColumnName = "ID")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name="EVALUATION_ID", referencedColumnName = "ID")
    private EvaluationEntity evaluation;

    @ManyToOne
    @JoinColumn(name="MATTER_ID", referencedColumnName = "ID")
    private MatterEntity matter;

    @Embedded
    private Qualification qualification;

    public QualificationUserEntity() {

    }

    public QualificationUserEntity(UserEntity user, EvaluationEntity evaluation, MatterEntity matter, Qualification qualification) {
        this.user = user;
        this.evaluation = evaluation;
        this.matter = matter;
        this.qualification = qualification;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getStudent() {
        return user;
    }

    public void setStudent(UserEntity user) {
        this.user = user;
    }

    public EvaluationEntity getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(EvaluationEntity evaluation) {
        this.evaluation = evaluation;
    }

    public MatterEntity getMatter() {
        return matter;
    }

    public void setMatter(MatterEntity matter) {
        this.matter = matter;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

}
