package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.RoleDao;
import com.posma.sckola.app.persistence.entity.RoleEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Repository("RoleDao")
public class RoleDaoImpl extends AbstractDao<RoleEntity> implements RoleDao {

    public RoleDaoImpl(){
        super();
        setClazz(RoleEntity.class);
    }

}
