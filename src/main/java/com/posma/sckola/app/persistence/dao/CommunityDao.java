package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.CommunityEntity;

import java.util.List;

/**
 * Created by Posma-ricardo on 10/04/2017.
 */
public interface CommunityDao {

    /**
     * description
     * @since 10/04/2017
     * @param id community identifier
     * @return CommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CommunityEntity findOne(long id);

    /**
     * description
     * @since 10/04/2017
     * @return List<CommunityEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<CommunityEntity> findAll();

    /**
     * description
     * @since 10/04/2017
     * @param community
     * @return CommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CommunityEntity create(CommunityEntity community);

    /**
     * description
     * @since 10/04/2017
     * @param community
     * @return CommunityEntity
     * @author PosmaGroup
     * @version 1.0
     */
    CommunityEntity update(CommunityEntity community);

    /**
     * description
     * @since 10/04/2017
     * @param community
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(CommunityEntity community);

    /**
     * description
     * @since 10/04/2017
     * @param id community identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
