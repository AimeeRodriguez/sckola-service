package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 11/04/2017.
 */
@Entity
@Table(name = "RATING_VALUE")
public class RatingValueEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "RATING_VALUE_SEQ", sequenceName = "S_ID_RATING_VALUE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RATING_VALUE_SEQ")
    private Long id;

    @Embedded
    private Qualification qualification;

    @ManyToOne
    @JoinColumn(name="RATING_SCALE_ID", nullable = false)
    private RatingScaleEntity ratingScale;

    public RatingValueEntity() {

    }

    public RatingValueEntity(Qualification qualification, RatingScaleEntity ratingScale) {
        this.qualification = qualification;
        this.ratingScale = ratingScale;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public RatingScaleEntity getRatingScale() {
        return ratingScale;
    }

    public void setRatingScale(RatingScaleEntity ratingScale) {
        this.ratingScale = ratingScale;
    }

}
