package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.AssistanceIntegralEntity;
import com.posma.sckola.app.persistence.entity.AssistanceIntegralPk;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface AssistanceIntegralDao {

    /**
     * description
     * @since 12/04/2017
     * @param id assistance identifier
     * @return AssistanceIntegralEntity
     * @author PosmaGroup
     * @version 1.0
     */
    AssistanceIntegralEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<AssistanceIntegralEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<AssistanceIntegralEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param assistance
     * @return AssistanceIntegralEntity
     * @author PosmaGroup
     * @version 1.0
     */
    AssistanceIntegralEntity create(AssistanceIntegralEntity assistance);

    /**
     * description
     * @since 12/04/2017
     * @param assistance
     * @return AssistanceIntegralEntity
     * @author PosmaGroup
     * @version 1.0
     */
    AssistanceIntegralEntity update(AssistanceIntegralEntity assistance);

    /**
     * description
     * @since 12/04/2017
     * @param assistance
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(AssistanceIntegralEntity assistance);

    /**
     * description
     * @since 12/04/2017
     * @param id assistance identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

    AssistanceIntegralEntity findOneAssistance(AssistanceIntegralPk assistanceIntegralPk);

    List<AssistanceIntegralEntity> findAllForSectionForDate(AssistanceIntegralPk assistanceIntegralPk);
    List<AssistanceIntegralEntity> noAttendanceFindAll(AssistanceIntegralPk assistanceIntegralPk);
}
