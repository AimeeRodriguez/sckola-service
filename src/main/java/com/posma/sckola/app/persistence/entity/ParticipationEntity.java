package com.posma.sckola.app.persistence.entity;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PARTICIPATION",
        indexes = {
                @Index(columnList ="ID",name = "IDX_ID_PARTICIPATION")
        })
public class ParticipationEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "PARTICIPATION_SEQ", sequenceName = "S_ID_PARTICIPATION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARTICIPATION_SEQ")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PARTICIPATION_ID", referencedColumnName = "ID", nullable = false)
    private InvitationEntity invitation;

    @Column(name = "NAME", nullable=true, length = 80)
    private String name;

    public ParticipationEntity(){}

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public InvitationEntity getInvitation() {
        return invitation;
    }

    public void setInvitation(InvitationEntity invitation) {
        this.invitation = invitation;
    }

}
