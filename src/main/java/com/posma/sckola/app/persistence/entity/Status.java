package com.posma.sckola.app.persistence.entity;


/**
 * Created by Francis on 24/04/2017.
 */
public enum Status {
    ACTIVE, INACTIVE
}
