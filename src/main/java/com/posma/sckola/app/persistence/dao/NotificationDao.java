package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.*;

import java.util.Date;
import java.util.List;

/**
 * Created by Posma-dev on 22/08/2017.
 */
public interface NotificationDao {


    /**
     * description
     * @since 27/03/2017
     * @param id user identifier
     * @return UserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    NotificationEntity findOne(long id);


    /**
     * description
     * @since 27/03/2017
     * @param date date of the notification
     * @param date date of the notification
     * @return UserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    NotificationEntity findByDateAndSectionAssistanceNotification(Date date, MatterCommunitySectionEntity mattersection);

    /**
     * description
     * @since 27/03/2017
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<NotificationEntity> findAll();

    /**
     * description
     * @since 27/03/2017
     * @param fieldName column name
     * @param id user identifier
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<NotificationEntity> findAllByFieldId(String fieldName, long id);

    /**
     * description
     * @since 27/03/2017
     * @param user
     * @return UserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    NotificationEntity create(NotificationEntity user);

    /**
     * description
     * @since 27/03/2017
     * @param user
     * @return UserEntity
     * @author PosmaGroup
     * @version 1.0
     */
    NotificationEntity update(NotificationEntity user);


    /**
     * description
     * @since 27/03/2017
     * @param user
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(NotificationEntity user);

    /**
     * description
     * @since 27/03/2017
     * @param id user identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);


    /**
     * Search notification by user (username)
     * @since 27/03/2017
     * @param username
     * @author Francis Ries
     * @version 1.0
     */
    List<NotificationEntity> findByUser(String username);


    /**
     * Search active notifications by user(username)
     * @param username
     * @return
     */
    List<NotificationEntity> findActiveNotificationByUserByCommunity(String username, Long communityId);

    /**
     * Search history notification by user
     * @param email User email to get notifications
     * @param communityId Community id to get notifications
     * @return List with all notifications, active and inactive ones.
     */
    List<NotificationEntity> findHistoryNotification(String email, Long communityId);

    /**
     * Searchs notification by a notification data.
     * @param notification notification to find.
     * @return List of notifications matched in database.
     */
    List<NotificationEntity> findByNotification(NotificationEntity notification);

    List<NotificationEntity> findActiveNotificationByUserByInfo(String username);

    List<NotificationEntity> findActiveNotificationByUser(String username, NotificationType type);


}
