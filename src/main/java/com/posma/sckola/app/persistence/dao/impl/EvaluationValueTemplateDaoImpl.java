package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationValueTemplateDao;
import com.posma.sckola.app.persistence.entity.EvaluationValueTemplateEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 27/04/2017.
 */

@Repository("EvaluationValueTemplateDao")
public class EvaluationValueTemplateDaoImpl extends AbstractDao<EvaluationValueTemplateEntity> implements EvaluationValueTemplateDao {

    public EvaluationValueTemplateDaoImpl(){
        super ();
        setClazz(EvaluationValueTemplateEntity.class);
    }


}
