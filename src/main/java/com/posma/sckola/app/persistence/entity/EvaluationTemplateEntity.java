package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Francis Ries on 27/04/2017.
 */
@Entity
@Table(name = "EVALUATION_TEMPLATE",
        indexes = {
            @Index(columnList ="DATE",name = "IDX_DATE_EVALUATION_TEMPLATE")
        })
public class EvaluationTemplateEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_TEMPLATE_SEQ", sequenceName = "S_ID_EVALUATION_TEMPLATE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_TEMPLATE_SEQ")
    private Long id;

    @Column(name = "OBJECTIVE", nullable=false, length = 255)
    private String objective;

  //  Se habilitara cuando se carge objetivos a una materia
  //  @Column(name = "CONTENT_TO_EVALUATE_ID", nullable=false)
  //  private Content content;

    @Column(name = "WEIGHT", nullable=true)
    private Integer weight;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "EVALUATION_TOOL_ID", referencedColumnName = "ID")
    private EvaluationToolEntity evaluationTool;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "EVALUATION_SCALE_ID", referencedColumnName = "ID")
    private RatingScaleEntity evaluationScale;

    @Column(name = "DATE", nullable=true)
    @Temporal(TemporalType.DATE)
    private Date date;

    @ManyToOne
    @JoinColumn(name="EVALUATION_PLAN_TEMPLATE_ID")
    private EvaluationPlanTemplateEntity evaluationPlanTemplate;

    @Column(name = "STATUS", nullable = false)
    private StatusEvaluation status = StatusEvaluation.PENDING;


    public EvaluationTemplateEntity() {

    }

    public EvaluationTemplateEntity(String objective, Integer weight) {
        this.objective = objective;
        this.weight = weight;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public EvaluationToolEntity getEvaluationTool() {
        return evaluationTool;
    }

    public void setEvaluationTool(EvaluationToolEntity evaluationTool) {
        this.evaluationTool = evaluationTool;
    }

    public RatingScaleEntity getEvaluationScale() {
        return evaluationScale;
    }

    public void setEvaluationScale(RatingScaleEntity evaluationScale) {
        this.evaluationScale = evaluationScale;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public EvaluationPlanTemplateEntity getEvaluationPlanTemplate() {
        return evaluationPlanTemplate;
    }

    public void setEvaluationPlanTemplate(EvaluationPlanTemplateEntity evaluationPlanTemplate) {
        this.evaluationPlanTemplate = evaluationPlanTemplate;
    }

    public StatusEvaluation getStatus(){return status;}

    public void setStatus(StatusEvaluation status){this.status = status;}

}
