package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.ConfMailEntity;

import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */
public interface ConfMailDao {

    /**
     * description 
     * @since 27/03/2017
     * @param id confMail identifier
     * @return ConfMailEntity
     * @author PosmaGroup
     * @version 1.0
     */
    ConfMailEntity findOne(long id);
    
    /**
     * description 
     * @since 27/03/2017
     * @return List<ConfMailEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<ConfMailEntity> findAll();

    /**
     * description 
     * @since 27/03/2017
     * @param fieldName column name
     * @param id confMail identifier
     * @return List<ConfMailEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<ConfMailEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description 
     * @since 27/03/2017
     * @param confMail
     * @return ConfMailEntity
     * @author PosmaGroup
     * @version 1.0
     */
    ConfMailEntity create(ConfMailEntity confMail);

    /**
     * description 
     * @since 27/03/2017
     * @param confMail
     * @return ConfMailEntity
     * @author PosmaGroup
     * @version 1.0
     */
    ConfMailEntity update(ConfMailEntity confMail);

    /**
     * description 
     * @since 27/03/2017
     * @param confMail
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(ConfMailEntity confMail);

    /**
     * description
     * @since 
     * @param id confMail identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

}
