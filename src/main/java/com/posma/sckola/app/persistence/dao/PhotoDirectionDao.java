package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.PhotoDirectionEntity;

import java.util.List;

/**
 * Created by Posma-ricardo on 13/07/2017.
 */
public interface PhotoDirectionDao {

    /**
     * description
     * @since 27/03/2017
     * @param id confMail identifier
     * @return ConfMailEntity
     * @author PosmaGroup
     * @version 1.0
     */
    PhotoDirectionEntity findOne(long id);

    /**
     * description
     * @since 27/03/2017
     * @return List<ConfMailEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<PhotoDirectionEntity> findAll();

    /**
     * description
     * @since 27/03/2017
     * @param fieldName column name
     * @param id confMail identifier
     * @return List<ConfMailEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<PhotoDirectionEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description
     * @since 27/03/2017
     * @param confMail
     * @return ConfMailEntity
     * @author PosmaGroup
     * @version 1.0
     */
    PhotoDirectionEntity create(PhotoDirectionEntity confMail);

    /**
     * description
     * @since 27/03/2017
     * @param confMail
     * @return ConfMailEntity
     * @author PosmaGroup
     * @version 1.0
     */
    PhotoDirectionEntity update(PhotoDirectionEntity confMail);

    /**
     * description
     * @since 27/03/2017
     * @param confMail
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(PhotoDirectionEntity confMail);

    /**
     * description
     * @since
     * @param id confMail identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
