package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 27/04/2017.
 */
@Entity
@Table(name = "EVALUATION_TOOL_TEMPLATE")
public class EvaluationToolTemplateEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_TOOL_TEMPLATE_SEQ", sequenceName = "S_ID_EVALUATION_TOOL_TEMPLATE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_TOOL_TEMPLATE_SEQ")
    private Long id;

    @Column(name = "NAME", nullable=false, length = 50)
    private String name;

    @Column(name = "DESCRIPTION", nullable=true, length = 512)
    private String description;


    public EvaluationToolTemplateEntity() {

    }

    public EvaluationToolTemplateEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
