package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.MatterDao;
import com.posma.sckola.app.persistence.entity.MatterEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("MatterDao")
public class MatterDaoImpl extends AbstractDao<MatterEntity> implements MatterDao {

    public MatterDaoImpl(){
        super ();
        setClazz(MatterEntity.class);
    }


}
