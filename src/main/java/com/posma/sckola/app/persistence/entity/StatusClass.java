package com.posma.sckola.app.persistence.entity;


/**
 * Created by Francis on 21/04/2017.
 *
 * ACTIVE: clase activa
 * INACTIVE: clase inactiva
 *
 */

public enum StatusClass {
    ACTIVE,INACTIVE
}
