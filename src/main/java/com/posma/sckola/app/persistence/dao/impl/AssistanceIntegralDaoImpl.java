package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.AssistanceIntegralDao;
import com.posma.sckola.app.persistence.entity.AssistanceIntegralEntity;
import com.posma.sckola.app.persistence.entity.AssistanceIntegralPk;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("AssistanceIntegralDao")
public class AssistanceIntegralDaoImpl extends AbstractDao<AssistanceIntegralEntity> implements AssistanceIntegralDao {

    public AssistanceIntegralDaoImpl(){
        super ();
        setClazz(AssistanceIntegralEntity.class);
    }

    public AssistanceIntegralEntity findOneAssistance(AssistanceIntegralPk assistanceIntegralPk){
        List<AssistanceIntegralEntity> assistanceByMatterEntityList = this.getEntityManager().createQuery("SELECT m FROM AssistanceIntegralEntity as m where m.id.user ='" + assistanceIntegralPk.getUser() + "' and m.id.sectionId ='" + assistanceIntegralPk.getSectionId() + "' and m.id.date ='" + assistanceIntegralPk.getDate() + "'", AssistanceIntegralEntity.class).getResultList();
        return assistanceByMatterEntityList.size() == 1 ? assistanceByMatterEntityList.get(0):null;
    }

    public List<AssistanceIntegralEntity> findAllForSectionForDate(AssistanceIntegralPk assistanceIntegralPk){
        Query query = this.getEntityManager().createQuery("SELECT m FROM AssistanceIntegralEntity as m where m.id.sectionId =:sectionId and m.id.date =:date");
        query.setParameter("sectionId", assistanceIntegralPk.getSectionId());
        query.setParameter("date", assistanceIntegralPk.getDate());
        return query.getResultList();
    }
    public List<AssistanceIntegralEntity> noAttendanceFindAll(AssistanceIntegralPk assistanceIntegralPk){
        Query query = this.getEntityManager().createQuery("SELECT m FROM AssistanceIntegralEntity as m where m.id.sectionId =:sectionId");
        query.setParameter("sectionId", assistanceIntegralPk.getSectionId());
        return query.getResultList();
    }

}
