package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.InvitationDao;
import com.posma.sckola.app.persistence.entity.InvitationEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("InvitationDao")
public class InvitationDaoImpl extends AbstractDao<InvitationEntity> implements InvitationDao {

    public InvitationDaoImpl(){
        super ();
        setClazz(InvitationEntity.class);
    }

    public InvitationEntity findByMail(String mail) {
        List<InvitationEntity> invitationEntityList = this.getEntityManager().createQuery("SELECT m FROM InvitationEntity as m where m.mail ='" + mail + "'", InvitationEntity.class).getResultList();
        return  invitationEntityList.size() == 1 ? invitationEntityList.get(0):null;
    }
}
