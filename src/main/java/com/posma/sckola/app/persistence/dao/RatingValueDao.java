package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.RatingValueEntity;

import java.util.List;

/**
 * Created by Francis Ries on 12/04/2017.
 */
public interface RatingValueDao {

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @return RatingValueEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RatingValueEntity findOne(long id);

    /**
     * description
     * @since 12/04/2017
     * @return List<RatingValueEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<RatingValueEntity> findAll();

    /**
     * description
     * @since 12/04/2017
     * @param ratingValue
     * @return RatingValueEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RatingValueEntity create(RatingValueEntity ratingValue);

    /**
     * description
     * @since 12/04/2017
     * @param ratingValue
     * @return RatingValueEntity
     * @author PosmaGroup
     * @version 1.0
     */
    RatingValueEntity update(RatingValueEntity ratingValue);

    /**
     * description
     * @since 12/04/2017
     * @param ratingValue
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(RatingValueEntity ratingValue);

    /**
     * description
     * @since 12/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
