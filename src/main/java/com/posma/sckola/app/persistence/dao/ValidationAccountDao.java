package com.posma.sckola.app.persistence.dao;


import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.persistence.entity.ValidationAccountEntity;

import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */
public interface ValidationAccountDao {

    /**
     * description 
     * @since 27/03/2017
     * @param validationCode validation Code
     * @return ValidationAccountEntity
     * @author Francis Ries
     * @version 1.0
     */
    ValidationAccountEntity findByValidationCode(String validationCode);

    /**
     * description
     * @since 27/03/2017
     * @param id validationAccount identifier
     * @return ValidationAccountEntity
     * @author PosmaGroup
     * @version 1.0
     */
    ValidationAccountEntity findOne(long id);

    /**
     * description 
     * @since 27/03/2017
     * @return List<ValidationAccountEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<ValidationAccountEntity> findAll();

    /**
     * description 
     * @since 27/03/2017
     * @param fieldName column name
     * @param id validationAccount identifier
     * @return List<ValidationAccountEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<ValidationAccountEntity> findAllByFieldId(String fieldName, long id);


    /**
     * description 
     * @since 27/03/2017
     * @param validationAccount
     * @return ValidationAccountEntity
     * @author PosmaGroup
     * @version 1.0
     */
    ValidationAccountEntity create(ValidationAccountEntity validationAccount);

    /**
     * description 
     * @since 27/03/2017
     * @param validationAccount
     * @return ValidationAccountEntity
     * @author PosmaGroup
     * @version 1.0
     */
    ValidationAccountEntity update(ValidationAccountEntity validationAccount);

    /**
     * description 
     * @since 27/03/2017
     * @param validationAccount
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(ValidationAccountEntity validationAccount);

    /**
     * description
     * @since 27/03/2017
     * @param id validationAccount identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);

    ValidationAccountEntity findByUserId(UserEntity userEntity);

}
