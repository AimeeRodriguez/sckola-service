package com.posma.sckola.app.persistence.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */
public abstract class AbstractDao<T extends Serializable> {
    /**
     * clazz
     */
    private Class<T> clazz;

    /**
     * entityManager
     */
    @PersistenceContext private EntityManager entityManager;

    /**
     * @return the javax.persistence.EntityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param clazz the clazz to set
     */
    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     *
     * @param id
     * @return
     */
    public T findOne(final long id) {
        return entityManager.find(clazz, id);
    }

    /**
     *
     * @return
     */
    @SuppressWarnings("unchecked") public List<T> findAll() {
        return entityManager.createQuery("from " + clazz.getName()).getResultList();
    }

    /**
     *
     * @param fieldName
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked") public List<T> findAllByFieldId(String fieldName, long id) {
        return entityManager.createQuery("from " + clazz.getName() + " where " + fieldName + "=" + id).getResultList();
    }


    /**
     *
     * @param fieldName
     * @param id
     * @return
     */
    @SuppressWarnings("unchecked") public List<T> findAllByTwoFieldsId(String fieldName, long id, String secondFieldName, long id2) {
        return entityManager.createQuery("from " + clazz.getName() + " where " + fieldName + "=" + id + " AND " + secondFieldName + "=" + id2).getResultList();
    }

    /**
     *
     * @param entity
     * @return
     */
    public T create(final T entity) {
        entityManager.persist(entity);
        return entity;
    }

    /**
     *
     * @param entity
     * @return
     */
    public T update(final T entity) {
        return entityManager.merge(entity);
    }

    /**
     *
     * @param entity
     */
    public void delete(final T entity) {
        entityManager.remove(entity);
    }

    /**
     *
     * @param entityId
     */
    public void deleteById(final long entityId) {
        final T entity = findOne(entityId);
        delete(entity);
    }

}
