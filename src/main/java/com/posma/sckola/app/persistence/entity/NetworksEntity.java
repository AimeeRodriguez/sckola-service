package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Posma-dev on 30/08/2018.
 */
@Entity
@Table(name = "SOCIAL_NETWORKS")
public class NetworksEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "SOCIAL_NETWORKS_SEQ", sequenceName = "S_ID_SOCIAL_NETWORKS", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOCIAL_NETWORKS_SEQ")
    private Long id;

    @Column(name = "PASSWORD", nullable=true, unique = true)
    private String password;

    @Column(name = "NETWORKS", nullable = false)
    private String networks;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
    private UserEntity user;

    public NetworksEntity(){

    }

    public Long getId(){ return id; }

    public void setId(Long id) { this.id = id;}

    public String getPassword(){ return password;}

    public void setPassword(String password){ this.password = password;}

    public String getNetworks() { return networks;}

    public void setNetworks(String networks){ this.networks = networks;}

    public UserEntity getUser(){ return user;}

    public void setUser(UserEntity user){ this.user = user;}

}
