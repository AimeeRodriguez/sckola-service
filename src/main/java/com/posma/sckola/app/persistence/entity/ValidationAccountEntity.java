package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Entity
@Table(name = "VALIDATION_ACCOUNT")
public class ValidationAccountEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @OneToOne (fetch = FetchType.LAZY, optional = false, cascade = CascadeType.MERGE)
    @JoinColumn (referencedColumnName = "ID", name = "USER_ID")
    private UserEntity userId;

    @Column(name = "VALIDATION_CODE", nullable=false)
    private String validationCode;

    @Column(name = "DATE", nullable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public ValidationAccountEntity() {
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
