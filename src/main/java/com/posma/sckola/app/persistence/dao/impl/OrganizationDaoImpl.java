package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.OrganizationDao;
import com.posma.sckola.app.persistence.entity.OrganizationEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */

@Repository("OrganizationDao")
public class OrganizationDaoImpl extends AbstractDao<OrganizationEntity> implements OrganizationDao {

    public OrganizationDaoImpl(){
        super ();
        setClazz(OrganizationEntity.class);
    }


    /**
     * description
     * @since 27/03/2017
     * @param businessName Business Name
     * @author Francis Ries
     * @version 1.0
     */
    public OrganizationEntity findByBusinessName(String businessName){
        List<OrganizationEntity> profileOrganizationEntities = this.getEntityManager().createQuery("select u from OrganizationEntity u " +
                "where u.businessName = '"+ businessName+"'", OrganizationEntity.class).getResultList();
        if(profileOrganizationEntities == null)
            return null;
        return  profileOrganizationEntities.size() > 0 ? profileOrganizationEntities.get(0):null;
    }

}
