package com.posma.sckola.config;

import com.posma.sckola.app.aspects.LoggingAspect;
import com.posma.sckola.app.aspects.PerformanceAspect;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.IOException;

/**
 * Created by francis on 27/03/2017.
 */
@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.posma.sckola.app")
public class AppConfig {

    final static Logger logger = Logger.getLogger(AppConfig.class);

    @Bean
    public LoggingAspect loggingAspect(){ return new LoggingAspect(); }

    @Bean
    public PerformanceAspect performanceAspect(){ return new PerformanceAspect(); }

    @Bean(name="multipartResolver")
    public CommonsMultipartResolver commonsMultipartResolver() throws IOException {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();

        //Set the maximum allowed size (in bytes) for each individual file.
        resolver.setMaxUploadSizePerFile(5242880);//5MB


        return resolver;
    }

}
