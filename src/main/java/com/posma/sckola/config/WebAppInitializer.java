package com.posma.sckola.config;

/**
 * Created by francis on 27/03/2017.
 */
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WebAppInitializer implements WebApplicationInitializer {

    public void onStartup(ServletContext servletContext) throws ServletException {
        WebApplicationContext rootContext = createRootContext(servletContext);

//        configureSpringMvc(servletContext, rootContext);

        //SimpleCorsFilter
        FilterRegistration.Dynamic corsFilter = servletContext.addFilter("corsFilter", SimpleCORSFilter.class);
        corsFilter.addMappingForUrlPatterns(null, false, "/*");

        //JwtFilter
        /*FilterRegistration.Dynamic jwtFilter = servletContext.addFilter("jwtTestingFilter", JwtAuthenticationTokenFilter.class);
        jwtFilter.addMappingForUrlPatterns(null, false, "*//*");*/
    }

    private WebApplicationContext createRootContext(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        //rootContext.register(WebSecurityConfig.class, JPAConfiguration.class, AppConfig.class);
        rootContext.register(JPAConfiguration.class, AppConfig.class);

        servletContext.addListener(new ContextLoaderListener(rootContext));
        servletContext.setInitParameter("defaultHtmlEscape", "true");

        return rootContext;
    }

//    private void configureSpringMvc(ServletContext servletContext, WebApplicationContext rootContext) {
//        AnnotationConfigWebApplicationContext mvcContext = new AnnotationConfigWebApplicationContext();
//        mvcContext.register(AppConfig.class);
//
//        mvcContext.setParent(rootContext);
//        ServletRegistration.Dynamic appServlet = servletContext.addServlet(
//                "dispatcher", new DispatcherServlet(mvcContext));
//        appServlet.addMapping("/");
//        //IMportante websockets
//        appServlet.setAsyncSupported(true);
//        appServlet.setLoadOnStartup(1);
//
//
//        /*Set<String> mappingConflicts = appServlet.addMapping("/api/*");
//
//        if (!mappingConflicts.isEmpty()) {
//            for (String s : mappingConflicts) {
//                LOG.error("Mapping conflict: " + s);
//            }
//            throw new IllegalStateException(
//                    "'webservice' cannot be mapped to '/'");
//        }*/
//    }


}